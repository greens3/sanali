/*
	Copyright © 2018 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the hearty_rabbit
    distribution for more details.
*/

//
// Created by nestal on 1/11/18.
//

#pragma once

#include <boost/iterator/iterator_adaptor.hpp>
#include <boost/range/iterator_range.hpp>

#include <boost/asio/any_completion_handler.hpp>
#include <boost/asio/buffers_iterator.hpp>
#include <boost/asio/bind_executor.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/write.hpp>
#include <boost/asio/strand.hpp>

#include <boost/beast/core/multi_buffer.hpp>
#include <boost/beast/core/buffers_prefix.hpp>
#include <boost/beast/core/buffers_to_string.hpp>
#include <boost/beast/core/buffers_cat.hpp>

#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <fmt/std.h>
#include <fmt/ranges.h>

#include <charconv>
#include <deque>
#include <memory>
#include <mutex>
#include <numeric>
#include <optional>
#include <string>
#include <string_view>
#include <type_traits>
#include <variant>
#include <vector>
#include <tuple>

#if __has_cpp_attribute(clang::reinitializes)
#define SANALI_REINIT [[clang::reinitializes]]
#else
#define SANALI_REINIT
#endif

namespace sanali {

inline constexpr std::string_view redis_port{"6379"};

enum class Error
{
	ok,
	parse_error,
	incorrect_type,
	command_error
};
struct ErrorCategory : std::error_category
{
	const char* name() const noexcept override{return "sanali";}
	std::string message(int ev) const override
	{
		switch (static_cast<Error>(ev))
		{
		case Error::ok:				return "OK";
		case Error::parse_error: 	return "parse error";
		case Error::incorrect_type: return "incorrect type";
		case Error::command_error:	return "redis command error";
		default: return "unknown";
		}
	}
};
inline const std::error_category& error_category()
{
	static const ErrorCategory cat;
	return cat;
}
inline auto make_error_code(Error error)
{
	return std::error_code{static_cast<int>(error), error_category()};
}
} // end of namespace sanali

namespace std {
template <>
struct is_error_code_enum<sanali::Error> : true_type {};
}

namespace sanali {
template <typename=void>
constexpr std::string_view split_left(std::string_view& in)
{
	// substr() will not throw even if "in" is empty and location==npos
	auto location = in.find(' ');
	auto result   = in.substr(0, location);

	in.remove_prefix(result.size());

	// Remove the matching characters, if any
	if (location != std::string_view::npos)
	{
		auto end = std::find_if(in.begin(), in.end(), [](char c){return c != ' ';});
		in.remove_prefix(end - in.begin());
	}
	return result;
}

template<typename Dependent, std::size_t>
using DependOn = Dependent;

template<typename T, std::size_t N, typename Indices = std::make_index_sequence<N>>
struct RepeatingTuple;

template<typename T, std::size_t N, std::size_t... Indices>
struct RepeatingTuple<T, N, std::index_sequence<Indices...>>
{
    using type = std::tuple<DependOn<T, Indices>...>;
};

template <typename ResultTuple, std::size_t remain>
constexpr std::size_t match_index()
{
	static_assert(remain < std::tuple_size<ResultTuple>::value);
	return std::tuple_size<ResultTuple>::value - remain - 1;
}

template <typename Field, typename Value, typename ResultTuple>
void match_field(ResultTuple& result, std::string_view name, Value&& value, Field field)
{
	if (name == field)
		std::get<match_index<ResultTuple, 0>()>(result) = std::forward<Value>(value);
}

template <typename... Fields, typename Field, typename Value, typename ResultTuple>
void match_field(ResultTuple& result, std::string_view name, Value&& value, Field field, Fields... remain)
{
	if (name == field)
		std::get<match_index<ResultTuple, sizeof...(remain)>()>(result) = std::forward<Value>(value);
	else
		match_field(result, name, std::forward<Value>(value), remain...);
}

// Need to extract replacement string without anything strings before and after
struct ExtractReplacementStringResult
{
	std::size_t pos;
	bool found;
};
template <typename=void>
constexpr ExtractReplacementStringResult extract_replacement_field(std::string_view field)
{
	int open_count = 0;
	bool found = false;
	std::size_t pos = 0;
	while (pos < field.size() && (!found || open_count > 0))
	{
		if (field[pos] == '{')
		{
			if (pos < field.size()-1 && field[pos+1] == '{')
			{
				++pos;
			}
			else
			{
				open_count++;
				found = true;
			}
		}
		else if (field[pos] == '}')
		{
			if (pos < field.size()-1 && field[pos+1] == '}')
				++pos;
			else if (open_count > 0)
				--open_count;
		}
		++pos;
	}
	return {pos, found};
}

class BufferList
{
public:
	// A small memory buffer suitable to store RESP array header.
	// All redis commands are represented as RESP arrays.
	// The size of the array is stored in the beginning as a string, e.g. "*100\r\n" where 100 is the array size.
	// We can use a smaller memory buffer to store the array header.
	// This buffer must be large enough to hold the array size (which is std::size_t) as
	// text, i.e. std::numeric_limits<std::size_t>::digits10. On top of that, 4 more bytes
	// are needed: '*', '\r', '\n' and the terminating null.
	// Since this class is used as a variant memory with std::string, it will occupy sizeof(std::string)
	// anyway even if it is smaller.
	class ArrayHeader
	{
		std::array<char, std::max(sizeof(std::string), std::numeric_limits<std::size_t>::digits10 + 4UL)> m_storage{};
	public:
		auto data() const {return m_storage.data();}
		auto size() const {return strnlen(m_storage.data(), m_storage.size());}
		void format(std::size_t array_size)
		{
			// The buffer should be large enough to hold the whole string. If this assertion fails,
			// the calculation of buffer size is wrong.
			// On the other hand, there won't be a buffer overflow even if we don't null-terminated
			// this string. strnlen() is used and it is bound to the size of the buffer.
			auto [out, bytes_needed] = fmt::format_to_n(m_storage.begin(), m_storage.size(), "*{}\r\n", array_size);
			assert(bytes_needed+1 < m_storage.size());
			assert(out != m_storage.end());
			*out = '\0';
		}
	};

	using value_type = std::variant<
		std::string,
		ArrayHeader,
		boost::asio::const_buffer,
		boost::beast::multi_buffer
	>;
private:
	using List              = std::vector<value_type>;
public:
	using iterator          = List::iterator;
	using const_iterator    = List::const_iterator;
	static constexpr std::size_t m_block_size = 80;

public:
	BufferList() = default;
	explicit BufferList(value_type&& item)
	{
		m_buffers.push_back(std::move(item));
	}

	void append(BufferList&& list)
	{
		for (auto&& buffer : list.m_buffers)
			std::visit([this](auto&& buf){append_buffer(std::forward<decltype(buf)>(buf));}, buffer);
		list.m_buffers.clear();
	}

	auto begin() {return m_buffers.begin();}
	auto end()   {return m_buffers.end();}
	auto begin() const {return m_buffers.begin();}
	auto end()   const {return m_buffers.end();}
	auto size()  const {return m_buffers.size();}
	bool empty() const {return m_buffers.empty();}
	SANALI_REINIT void clear() {m_buffers.clear();}

	auto& front() {return m_buffers.front();}
	auto& back() {return m_buffers.back();}

	[[nodiscard]] auto total_size() const
	{
		return std::accumulate(m_buffers.begin(), m_buffers.end(), std::size_t{}, [](std::size_t s, auto&& val)
		{
			return std::visit([](auto&& buf){return buf.size();}, val) + s;
		});
	}

	[[nodiscard]] auto buffers() const
	{
		std::vector<boost::asio::const_buffer> result;
		for (auto&& val : m_buffers)
		{
			std::visit([&result](auto&& buf)
			{
				if constexpr (std::is_same_v<std::decay_t<decltype(buf)>, boost::beast::multi_buffer>)
					for (auto&& b : buf.cdata())
						result.push_back(b);

				else
					result.emplace_back(buf.data(), buf.size());
			}, val);
		}
		assert(boost::beast::buffer_bytes(result) == total_size());
		return result;
	}

	template <typename Buffer>
	void store_buffer(Buffer&& buffer)
	{
		m_buffers.emplace_back(std::forward<Buffer>(buffer));
	}

	void merge_buffer(const boost::beast::multi_buffer& buffer)
	{
		auto buffer_data = buffer.data();
		for (auto piece : buffer_data)
			merge_buffer(piece);
	}

	template <typename Buffer>
	void append_buffer(Buffer&& buffer)
	{
		if (!m_buffers.empty())
		{
			// If it has enough space, extend the std::string at the end of our list.
			// Then merge the incoming buffer with the std::string at the end.
			if (auto last = std::get_if<std::string>(&m_buffers.back());
				last && last->size() + buffer.size() <= last->capacity())
			{
				return merge_buffer(std::forward<Buffer>(buffer));
			}
		}
		store_buffer(std::forward<Buffer>(buffer));
	}

	template <typename Buffer, typename=std::enable_if_t<
		(
			std::is_convertible_v<std::decay_t<Buffer>, boost::asio::const_buffer> ||
			std::is_same_v<std::decay_t<Buffer>, std::string> ||
			std::is_same_v<std::decay_t<Buffer>, ArrayHeader>
		) &&
		!std::is_same_v<std::decay_t<Buffer>, boost::beast::multi_buffer>
	>>
	void merge_buffer(Buffer&& buffer)
	{
		auto data = static_cast<const char*>(buffer.data());
		last(std::max(buffer.size(), m_block_size)).append(data, data + buffer.size());
	}

	std::string& last(std::size_t block_size = m_block_size)
	{
		if (m_buffers.empty() || !std::holds_alternative<std::string>(m_buffers.back()))
		{
			std::string tmp;
			tmp.reserve(block_size);
			m_buffers.emplace_back(std::move(tmp));
			assert(std::get<std::string>(m_buffers.back()).capacity() >= block_size);
		}

		return std::get<std::string>(m_buffers.back());
	}

private:
	List	m_buffers;
};

struct BufferRef
{
	boost::asio::const_buffer buf;
};
template <std::size_t N>
auto buffer_ref(const char (&buf)[N])
{
	return BufferRef{boost::asio::const_buffer{buf, strnlen(buf, N)}};
}

/// Convert a format string and a list of arguments that corresponds to the replacement strings "{}"
/// into a list of buffers. The list of arguments can be passed one-by-one via try_format(). They
/// can also be passed as a complete list via format().
class BufferFormatter
{
public:
	explicit BufferFormatter(std::string_view fmt, std::string&& init = std::string{}) :
		m_format_string{fmt}, m_buffers{std::move(init)}
	{
	}

	template <typename Arg, typename ... Args>
	void format(Arg&& arg, Args && ... args)
	{
		if (m_format_string.empty())
			commit();
		else if (try_format(std::forward<Arg>(arg)))
			format(std::forward<Args>(args)...);
		else
			format(std::forward<Arg>(arg), std::forward<Args>(args)...);
	}

	void format()
	{
		commit();
	}

	template <typename Arg>
	bool try_format(Arg && arg)
	{
		assert(!m_format_string.empty());
		auto [pos, found] = extract_replacement_field(m_format_string);
		auto rs = m_format_string.substr(0, pos);
		m_format_string.remove_prefix(pos);
		if (found)
		{
			if constexpr (std::is_same_v<std::decay_t<Arg>, boost::beast::multi_buffer> ||
		                  std::is_same_v<std::decay_t<Arg>, BufferRef>  ||
						  std::is_same_v<std::decay_t<Arg>, BufferList> ||
			              std::is_convertible_v<std::decay_t<Arg>, boost::asio::const_buffer>)
			{
				auto& current = m_buffers.last();

				// Use an empty string to replace the replacement field. We will
				// put the buffer itself in the buffer list instead of formatting it.
				fmt::format_to(std::back_inserter(current), fmt::runtime(rs), "");

				// multi_buffers owns the memory. Copying them will deep-copy. Moving them will
				// transfer the ownership. Both copying and moving works well for async write.
				if constexpr (std::is_same_v<std::decay_t<Arg>, boost::beast::multi_buffer>)
					m_buffers.append_buffer(std::forward<Arg>(arg));

				// The caller uses a BufferRef to wrap the buffer. It means that the caller
				// can make sure the buffer won't be released throughout the async write. We can safely
				// store the const_buffer object without deep copying the actual bytes.
				else if constexpr (std::is_same_v<std::decay_t<Arg>, BufferRef>)
					m_buffers.store_buffer(arg.buf);

				// Since const_buffer does not own the memory, storing them will not make a deep copy.
				// For simplicity just merge them. If it is too slow we will find another way.
				else if constexpr (std::is_convertible_v<std::decay_t<Arg>, boost::asio::const_buffer>)
					m_buffers.merge_buffer(std::forward<Arg>(arg));

				else if constexpr (std::is_same_v<std::decay_t<Arg>, BufferList>)
					m_buffers.append(std::forward<Arg>(arg));

				else
					static_assert(!std::is_same_v<Arg,Arg>, "Buffer type unsupported");
			}
			else
			{
				fmt::format_to(std::back_inserter(m_buffers.last()), fmt::runtime(rs), std::forward<Arg>(arg));
			}
			return true;
		}

		// If replacement string "{}" is not found, then "arg" is an argument for the
		// next replacement string in the format string.
		else
		{
			assert(m_format_string.empty());
			fmt::format_to(std::back_inserter(m_buffers.last()), fmt::runtime(rs));
			return false;
		}
	}

	[[nodiscard]] bool finished() const {return m_format_string.empty();}

	void commit()
	{
		// Store the last part of the field
		fmt::format_to(std::back_inserter(m_buffers.last()), fmt::runtime(std::exchange(m_format_string, std::string_view{})));
		assert(finished());
	}

	[[nodiscard]] auto total_size() const {return m_buffers.total_size();}

	auto& storage() {return m_buffers;}

private:
	std::string_view	m_format_string;
	BufferList	        m_buffers;
};

template <typename ... Args>
auto format(std::string_view field, Args && ... args)
{
	BufferFormatter formatter{field};
	formatter.format(std::forward<Args>(args)...);
	return BufferList{std::move(formatter.storage())};
}

class CommandBuilder
{
private:
	using ArrayHeader = BufferList::ArrayHeader;

public:
	CommandBuilder(CommandBuilder&&) = default;
	CommandBuilder& operator=(CommandBuilder&&) = default;

	template <std::size_t N, typename ... Args>
	explicit CommandBuilder(const char (&cmd_string)[N], Args && ... args)
	{
		assign(cmd_string, std::forward<Args>(args)...);
	}

	template <std::size_t N, typename ... Args>
	SANALI_REINIT auto& assign(const char (&cmd_string)[N], Args && ... args)
	{
		m_string_count = 0;
		m_buffers.clear();
		m_buffers.store_buffer(ArrayHeader{});

		command_impl(std::string_view{cmd_string, strnlen(cmd_string, N)}, std::forward<Args>(args)...);
		update_array_header();

		return *this;
	}

	/// Append one argument to the command.
	/// Every time when an append() function is called, string_count() will increase by 1.
	/// Spaces within the argument of append() is treated as normal data and has no special meaning.
	auto& append(BufferList&& fields = {}) &
	{
		// assign() is called in constructor, so m_buffers should contain at least
		// two buffers: the array header and the command itself.
		assert(m_buffers.size() >= 2);

		// Append the RESP string to the last buffer and update array size
		write_resp_string(std::move(fields));
		update_array_header();

		// For chaining calls.
		return *this;
	}
	auto&& append(BufferList&& fields = {}) && {return std::move(append(std::move(fields)));}

	template <typename ... Args>
	auto& append(std::string_view field, Args && ... args) &
	{
		return append(format(field, std::forward<Args>(args)...));
	}
	template <typename ... Args>
	auto&& append(std::string_view field, Args && ... args) &&
	{
		return std::move(append(field, std::forward<Args>(args)...));
	}

	// This is too frequently used, so it deserves a shortcut.
	template <typename Arg, std::enable_if_t<
	    !std::is_convertible_v<std::decay_t<Arg>, std::string_view>
    >* = nullptr>
	auto& append(Arg&& field) &
	{
		static_assert(!std::is_convertible_v<std::decay_t<Arg>, std::string_view>);
		return append("{}", std::forward<Arg>(field));
	}
	template <typename Arg, std::enable_if_t<
		!std::is_convertible_v<std::decay_t<Arg>, std::string_view>
	>* = nullptr>
	auto&& append(Arg&& field) &&
	{
		return std::move(append(std::forward<Arg>(field)));
	}

	[[nodiscard]] auto string_count() const {return m_string_count;}

	[[nodiscard]] auto as_string() const
	{
		return boost::beast::buffers_to_string(m_buffers.buffers());
	}

	[[nodiscard]] auto& buffers() const {return m_buffers;}
	[[nodiscard]] auto& buffers() {return m_buffers;}

private:
	// The objective of this function is to convert the command string "fmt"
	// into an array of RESP bulk strings.
	// The command string is made up of multiple "fields", separated by spaces.
	// Each field is a string element of the resultant array.
	// The fields can have replacement strings (e.g. {}). These replacement
	// strings will be replaced by "args" using libfmt.
	template <typename ... Args>
	void command_impl(std::string_view fmt, Args && ... args)
	{
		while (!fmt.empty())
		{
			// Try to extract one field from the command string and format it
			const auto field = split_left(fmt);
			if (extract_replacement_field(field).found)
			{
				// The first buffer is the array header.
				assert(!m_buffers.empty());
				assert(std::holds_alternative<ArrayHeader>(m_buffers.front()));

				// The first field must not be a replacement field
				if (m_buffers.size() == 1)
					throw std::runtime_error("The first field must not be a replacement field");

				BufferFormatter current{field};
				return format_one_field(fmt, current, std::forward<Args>(args)...);
			}

			// If the field does not contain a replacement string "{}
			// we can store it in the output array as an element.
			else
			{
				BufferFormatter current{field};
				current.commit();
				write_resp_string(std::move(current.storage()));
			}
		}
	}

	// If "fmt" is the string "SET key{} value_of_{}". In this example, we have three fields.
	// "current" is the formatted result of one field, e.g. "key{}". BufferFormatter::try_format() will
	// replace {} in the field with "arg" one-by-one.
	template <typename Arg, typename ... Args>
	void format_one_field(
		std::string_view fmt,
		BufferFormatter& current,
		Arg && arg, Args && ... args
	)
	{
		bool arg_consumed = current.try_format(std::forward<Arg>(arg));

		// The current field has already finished. Flush the buffers in the fields to the buffer storage.
		if (current.finished())
		{
			current.commit();
			write_resp_string(std::move(current.storage()));

			if (arg_consumed)
				command_impl(fmt, std::forward<Args>(args)...);
			else
				command_impl(fmt, std::forward<Arg>(arg), std::forward<Args>(args)...);
		}
		else
		{
			if (arg_consumed)
				format_one_field(fmt, current, std::forward<Args>(args)...);
			else
				format_one_field(fmt, current, std::forward<Arg>(arg), std::forward<Args>(args)...);
		}
	}

	// End of recursion. No more argument to format.
	void format_one_field(std::string_view fmt, BufferFormatter& current)
	{
		current.commit();
		write_resp_string(std::move(current.storage()));
		command_impl(fmt);
	}

	void write_resp_string(BufferList&& bulk_str)
	{
		// this is the "header" of the bulk string, i.e. its size
		fmt::format_to(std::back_inserter(m_buffers.last()), "${}\r\n", bulk_str.total_size());

		// store the bulk string "body"
		m_buffers.append(std::move(bulk_str));

		// this will be the "footer" of the bulk string
		m_buffers.last().append(std::string_view{"\r\n"});
		++m_string_count;
	}

	// Update array header with the new array size
	void update_array_header()
	{
		assert(m_buffers.size() >= 2);
		assert(std::holds_alternative<ArrayHeader>(m_buffers.front()));
		std::get<ArrayHeader>(m_buffers.front()).format(m_string_count);
	}

private:
	std::size_t m_string_count{};
	BufferList  m_buffers;
};

class Connection;

// https://en.cppreference.com/w/cpp/utility/variant/visit
// Must-have for variants
template<class... Ts>
struct Overloaded : Ts... { using Ts::operator()...; };
template<class... Ts>
Overloaded(Ts...) -> Overloaded<Ts...>;

class Resp;
std::string format_as(const Resp& resp);

using Handler = boost::asio::any_completion_handler<void(Resp&&, std::error_code)>;
using SAX = std::function<void(void*, std::size_t, std::size_t, std::error_code)>;

struct Consumer : std::variant<std::monostate, Handler, SAX>
{
	// std::monostate
	Consumer() = default;

	// enable_if_t to silence clang-tidy
	template <typename F, typename=std::enable_if_t<!std::is_same_v<std::decay_t<F>, Consumer>>>
	explicit Consumer(F&& func)
	{
		if constexpr (std::is_invocable_v<F, Resp&&, std::error_code>)
			emplace<Handler>(std::forward<F>(func));
		else if constexpr (std::is_invocable_v<F, boost::beast::multi_buffer&&, std::error_code>)
			emplace<SAX>(create_SAX_from_bmi_callback(std::forward<F>(func)));
		else if constexpr (std::is_invocable_v<F, void*, std::size_t, std::size_t, std::error_code>)
			emplace<SAX>(std::forward<F>(func));
		else if constexpr (std::is_same_v<F, std::monostate>)
			emplace<std::monostate>();
		else
			static_assert(!std::is_same_v<F,F>, "Consumer callback type unsupported");
	}

	void consume(Resp&& value, std::error_code ec);
	bool use_sax() const {return std::holds_alternative<SAX>(*this);}

	template <typename MutableBuffers>
	void callback_sax(MutableBuffers&& buffers, std::size_t total_size)
	{
		assert(std::holds_alternative<SAX>(*this));
		for (auto&& piece : buffers)
			std::get<SAX>(*this)(piece.data(), piece.size(), total_size, {});
	}

	Consumer& bind_executor(boost::asio::any_io_executor exec) &;
	auto&& bind_executor(boost::asio::any_io_executor exec) && {return std::move(bind_executor(exec));}

	template <typename MultiBufferCallback>
	auto create_SAX_from_bmi_callback(MultiBufferCallback&& callback)
	{
		return [
			buffer=std::make_shared<boost::beast::multi_buffer>(), callback=std::forward<MultiBufferCallback>(callback)
		](const void* data, std::size_t size, std::size_t total, std::error_code ec) mutable
		{
			assert(buffer);
			boost::asio::buffer_copy(buffer->prepare(size), boost::asio::const_buffer{data, size});
			buffer->commit(size);
			if (buffer->size() == total)
				callback(std::move(*buffer), ec);
		};
	}
};
static_assert(!std::is_copy_constructible_v<Consumer>);
static_assert(std::is_move_constructible_v<Consumer>);

struct RespString {std::string val; auto operator==(const RespString& a) const {return val == a.val;}};
struct RespError {std::string val; auto operator==(const RespError& a) const {return val == a.val;}};
struct RespInt {std::int64_t val; auto operator==(const RespInt& a) const {return val == a.val;}};
struct RespArray {std::vector<Resp> val;};

///	Result of Redis command.
///	The Resp class mainly served as a union type to represent the result of a redis command.
///	It can be only the the following types: nil, strings, errors, integers and array.
///	Different redis commands have results in different data types. Refer to redis documentation
/// to find out the data type of a given redis command. Note that if an error is encountered, the
/// data type may be different. Typically, the "error" type is used for errors.
class Resp
{
public:
	constexpr Resp() = default;
	explicit Resp(RespArray arr) : m_val{std::move(arr)} {}
	explicit Resp(RespError err) : m_val{std::move(err)} {}
	explicit Resp(std::string s) : m_val{RespString{std::move(s)}} {}

	template <typename Integer, typename=std::enable_if_t<std::is_integral_v<Integer>>>
	explicit Resp(Integer i) : m_val{RespInt{i}} {}

	template <typename F>
	auto visit(F&& func) const {return std::visit(std::forward<F>(func), m_val);}

	// Response types
	[[nodiscard]] bool is_nil() const noexcept {return std::holds_alternative<std::monostate>(m_val);}
	[[nodiscard]] bool is_int() const noexcept {return std::holds_alternative<RespInt>(m_val);}
	[[nodiscard]] bool is_string() const noexcept {return std::holds_alternative<RespString>(m_val);}
	[[nodiscard]] bool is_error() const noexcept {return std::holds_alternative<RespError>(m_val);}
	[[nodiscard]] bool is_array() const noexcept {return std::holds_alternative<RespArray>(m_val);}
	[[nodiscard]] explicit operator bool() const noexcept {return !is_error() && !is_nil();}

	// The from_chars() function will try to convert the underlying data type to the requested type.
	// They don't throw exception when the type request is different. Even if the requested type
	// is the same and no conversion is required, they still need to copy the value. In particular,
	// to_string() need to engage the formatter to convert int/string/array into strings. It is much
	// slower than str() or as_string().
	template <typename Numeric=std::int64_t>
	[[nodiscard]] auto from_chars(std::error_code& ec, Numeric def_value = 0) const noexcept
	{
		return std::visit(Overloaded{
			[](const RespInt& s){return static_cast<Numeric>(s.val);},
			[&ec, def_value](const RespString& s) mutable
			{
#ifdef __clang__
				try
				{
					return static_cast<Numeric>(std::stod(s.val));
				}
				catch (std::invalid_argument&) {ec = make_error_code(std::errc::invalid_argument);}
				catch (std::out_of_range&)     {ec = make_error_code(std::errc::result_out_of_range);}
#else
				// from_chars() will not change def_value if failed
				if (auto r = std::from_chars(s.val.data(), s.val.data() + s.val.size(), def_value);
					r.ec != std::errc{})
					ec = make_error_code(r.ec);
				else if (r.ptr != s.val.data() + s.val.size())
				{
					auto db = static_cast<double>(def_value);
					r = std::from_chars(s.val.data(), s.val.data() + s.val.size(), db);
					if (r.ec != std::errc{})
						ec = make_error_code(r.ec);
					def_value = static_cast<Numeric>(db);
				}
#endif
				return def_value;
			},
			[&ec, def_value](auto&&) {ec = Error::incorrect_type; return def_value;;}
		}, m_val);
	}

	// The to_*() functions are shortcuts to from_chars().

	// Sometimes redis return "1.23e6" even for "integers". We better use the double
	// version of from_chars().
	auto to_int(std::int64_t def_value = 0) const noexcept
	{
		std::error_code ec;
		return from_chars(ec, def_value);
	}
	auto to_int(std::error_code& ec) const noexcept
	{
		return from_chars(ec, std::int64_t{});
	}
	auto to_double(double def_value = 0) const noexcept
	{
		std::error_code ec;
		return from_chars(ec, def_value);
	}
	auto to_double(std::error_code& ec) const noexcept
	{
		return from_chars(ec, 0.0);
	}
	std::string to_string() const {return format_as(*this);}

	// The as_*(T) function will not throw if the requested type is different from the actual type.
	// Instead, the default value (specified by the first parameter) will be returned.
	auto  as_int(std::int64_t def)               const {auto v = std::get_if<RespInt>(&m_val);    return v?v->val:def;}
	auto& as_string(const std::string& def)      const {auto v = std::get_if<RespString>(&m_val); return v?v->val:def;}
	auto& as_error(const std::string& def)       const {auto v = std::get_if<RespError>(&m_val);  return v?v->val:def;}
	auto& as_array(const std::vector<Resp>& def) const {auto v = std::get_if<RespArray>(&m_val);  return v?v->val:def;}

	static const Resp& nil() {static const Resp empty{}; return empty;}

	// The as_*(void) functions will throw if the request type is different from the actual type.
	// They return references to the underlying type, so they do not copy the value.
	[[nodiscard]] auto as_int()     const {return std::get<RespInt>(m_val).val;}
	[[nodiscard]] auto& as_string()       {return std::get<RespString>(m_val).val;}
	[[nodiscard]] auto& as_string() const {return std::get<RespString>(m_val).val;}
	[[nodiscard]] auto& as_error()        {return std::get<RespError>(m_val).val;}
	[[nodiscard]] auto& as_error()  const {return std::get<RespError>(m_val).val;}
	[[nodiscard]] auto& as_array()        {return std::get<RespArray>(m_val).val;}
	[[nodiscard]] auto& as_array()  const {return std::get<RespArray>(m_val).val;}
	[[nodiscard]] const Resp& as_array(std::size_t i) const noexcept
	{
		auto array = std::get_if<RespArray>(&m_val);
		return array && i < array->val.size() ? array->val[i] : nil();

	}
	[[nodiscard]] const Resp& as_array(std::size_t i, std::error_code& ec) const noexcept
	{
		auto array = std::get_if<RespArray>(&m_val);
		if (!array || i >= array->val.size())
		{
			ec = make_error_code(Error::incorrect_type);
			return nil();
		}
		return array->val[i];
	}
	[[nodiscard]] const Resp& operator[](std::size_t i) const noexcept {return as_array(i);}
	[[nodiscard]] std::size_t array_size() const noexcept
	{
		auto array = std::get_if<RespArray>(&m_val);
		return array ? array->val.size() : 1UL;
	}

	using reference = std::vector<Resp>::reference;
	using iterator = std::vector<Resp>::const_iterator;
	using const_iterator = std::vector<Resp>::const_iterator;
	[[nodiscard]] iterator begin() const noexcept
	{
		auto array = std::get_if<RespArray>(&m_val);
		return array ? array->val.begin() : iterator{};
	}
	[[nodiscard]] iterator end() const noexcept
	{
		auto array = std::get_if<RespArray>(&m_val);
		return array ? array->val.end() : iterator{};
	}

	class KeyValue
	{
	public:
		explicit KeyValue(iterator current) : m_current{current} {}

		[[nodiscard]] auto key() const {return m_current->as_string();}
		[[nodiscard]] auto value() const {auto i = m_current; return *++i;}

	private:
		iterator m_current;
	};

	class kv_iterator : public boost::iterator_adaptor<
	    kv_iterator,
		iterator,
		KeyValue,
		boost::random_access_traversal_tag,
		KeyValue
	>
	{
	public:
		kv_iterator() = default;
		explicit kv_iterator(iterator it) : kv_iterator::iterator_adaptor_{it} {}
	private:
		friend class boost::iterator_core_access;
		void increment() {this->base_reference() += 2;}
		[[nodiscard]] auto dereference() const {return KeyValue{this->base()};}
		[[nodiscard]] auto distance_to(kv_iterator other) const
		{
			return (other.base() - base()) / 2;
		}
		void advance(typename iterator_adaptor::difference_type n)
		{
			this->base_reference() += n*2;
		}
	};

	[[nodiscard]] auto kv_pairs() const noexcept
	{
		return boost::iterator_range<kv_iterator>{
			kv_iterator{begin()},
			kv_iterator{end()}
		};
	}

	// Return a tuple of replies, one for each field in the parameter list
	template <typename... Field>
	auto map_kv_pair(Field... fields) const
	{
		typename RepeatingTuple<Resp, sizeof...(fields)>::type result;
		for (auto i = 0U; i+1 < array_size() ; i += 2)
			match_field(result, as_array(i).as_string(), as_array(i+1), fields...);
		return result;
	}

	/// Return a tuple of the first \a count replies in the array.
	template <std::size_t count>
	auto as_tuple(std::error_code& ec) const
	{
		return as_tuple_impl(ec, std::make_index_sequence<count>{});
	}

	friend bool operator==(const Resp& lhs, const Resp& rhs);

	// As many redis commands returns "OK" on success, ok() is provided as a shortcut.
	// Note that it is case-sensitive.
	bool ok() const {return is_string() && as_string() == "OK";}

private:
	template <std::size_t... index>
	auto as_tuple_impl(std::error_code& ec, std::index_sequence<index...>) const
	{
		return std::make_tuple(as_array(index, ec)...);
	}

	std::variant<std::monostate, RespString, RespError, RespInt, RespArray> m_val;
};

inline void Consumer::consume(Resp&& value, std::error_code ec)
{
	if (!ec && value.is_error())
		ec = Error::command_error;

	std::visit(Overloaded{
		[](std::monostate){},
		[&](Handler& func){func(std::move(value), ec);},
		[&](const SAX& func)
		{
			if (!ec)
				ec = make_error_code(Error::incorrect_type);

			// Convert the variant into string and pass to the SAX handler.
			// Hopefully, it will be logged.
			auto str = value.to_string();
			func(str.data(), str.size(), str.size(), ec);
		}
	}, *this);
}

inline Consumer& Consumer::bind_executor(boost::asio::any_io_executor exec) &
{
	if (auto sax = std::get_if<SAX>(this); sax)
	{
		emplace<SAX>([
			exec=std::move(exec), sax=*sax
		](void* data, std::size_t size, std::size_t total, std::error_code ec)
		{
			std::vector<std::uint8_t> copy(static_cast<std::uint8_t*>(data), static_cast<std::uint8_t*>(data)+size);
			boost::asio::dispatch(exec, [total, ec, copy=std::move(copy), sax]() mutable
			{
				sax(copy.data(), copy.size(), total, ec);
			});
		});
	}
	else if (auto handler = std::get_if<Handler>(this); handler)
	{
		emplace<Handler>(boost::asio::bind_executor(exec, [handler=std::move(*handler)](Resp&& resp, std::error_code ec) mutable
		{
			handler(std::move(resp), ec);
		}));
	}
	return *this;
}


// Comparison with different types
inline bool operator==(const RespArray& lhs, const RespArray& rhs){return lhs.val   == rhs.val  ;}
inline bool operator==(const Resp&      lhs, const Resp&      rhs){return lhs.m_val == rhs.m_val;}
inline bool operator==(const Resp&      lhs, std::string_view rhs){return lhs.is_string() && lhs.as_string() == rhs;}
inline bool operator==(std::string_view lhs, const Resp&      rhs){return rhs == lhs;}
inline bool operator==(const Resp&      lhs, std::int64_t     rhs){return lhs.is_int() && lhs.as_int() == rhs;}
inline bool operator==(std::int64_t     lhs, const Resp&      rhs){return rhs == lhs;}
inline bool operator!=(const Resp&      lhs, const Resp&      rhs){return !(lhs == rhs);}
template <typename RHS>
inline bool operator!=(const Resp& lhs, const RHS& rhs)
{
	return !operator==(lhs, rhs);
}
template <typename LHS>
inline bool operator!=(const LHS& lhs, const Resp& rhs)
{
	return !operator==(lhs, rhs);
}

class RespParser
{
public:
	explicit RespParser(std::size_t min_sax_size = 0) : m_min_sax_size{min_sax_size} {}

	// Get a bunch of mutable buffers for reading. The return value can be pass to async_read().
	auto prepare(std::size_t size)
	{
		return m_buffer.prepare(size);
	}

	// To be called after async_read() finishes reading. Indicate the number of bytes in the
	// buffers returned by the last call to prepare() has been filled. These data will be
	// parsed by a subsequent call to parse().
	void commit(std::size_t bytes_read)
	{
		m_buffer.commit(bytes_read);
	}

	template <typename Container>
	void push(const Container& cond)
	{
		auto buffer = prepare(cond.size());
		auto data   = static_cast<const char*>(cond.data());
		std::copy(data, data+cond.size(), boost::asio::buffers_begin(buffer));
		commit(cond.size());
	}
	void push(const char* str){push(std::string_view{str});}

	void set_min_sax_size(std::size_t size)
	{
		m_min_sax_size = size;
	}

	// Tries to exact a result from m_buffer and let the "consumer" consume it.
	// Return true if the consumer has consumed the result. Otherwise false.
	bool parse(Consumer& consumer)
	{
		constexpr std::string_view newline{"\r\n"};

		// Each consumer can only consume exactly one result of one command.
		// The loop is done if the consumer has consumed the result.
		bool result_consumed{false};

		while (!result_consumed && m_buffer.size() > 0 && bulk_string_ready_for_consume(consumer))
		{
			if (m_bulk_str)
			{
				result_consumed = parse_bulk_string(consumer);
				continue;
			}

			auto buf = m_buffer.data();
			auto it = std::search(
				boost::asio::buffers_begin(buf),
				boost::asio::buffers_end(buf),
				newline.begin(), newline.end()
			);
			if (it == boost::asio::buffers_end(buf))
				break;

			// Except for bulk strings (which is handled above), other
			// data types should be short enough to be stored in a std::string
			std::string line{boost::asio::buffers_begin(buf), it};
			m_buffer.consume(line.size() + newline.size());

			if (!line.empty())
				result_consumed = parse_line(std::move(line), consumer);
		}
		return result_consumed;
	}

	std::optional<Resp> parse()
	{
		Resp result;
		Consumer consumer{[&result](auto&& resp, auto ec)
		{
			if (!ec || resp.is_error())
				result = std::move(resp);
		}};
		if (parse(consumer))
			return result;
		else
			return std::nullopt;
	}

private:
	bool parse_line(std::string&& line, Consumer& consumer)
	{
		assert(!m_bulk_str);
		auto to_int = [&line](auto& val)
		{
			return std::from_chars(line.data()+1, line.data()+line.size(), val).ec == std::errc{};
		};

		assert(!line.empty());
		switch (line.front())
		{
		// array
		case '*':
			if (std::int64_t size_int{}; to_int(size_int))
			{
				// Arrays with negative size are treated as nils.
				if (size_int < 0)
					return on_resp_complete({}, consumer);

				// Empty array are considered "finished" because there is nothing inside
				if (size_int == 0)
					return on_resp_complete(Resp{RespArray{}}, consumer);

				m_arrays.emplace_back().expected_size = static_cast<std::size_t>(size_int);
			}
			break;
		case '+':
			return on_resp_complete(Resp{std::move(line).substr(1)}, consumer);
		case '-':
			return on_resp_complete(Resp{RespError{std::move(line).substr(1)}}, consumer);
		case ':':
			if (std::int64_t val{}; to_int(val))
				return on_resp_complete(Resp{val}, consumer);
			break;
		case '$':
			if (std::int64_t val{}; to_int(val))
			{
				// Bulk strings with negative size are treated as nils.
				if (val < 0)
					return on_resp_complete({}, consumer);

				assert(!m_bulk_str);
				m_bulk_str = BulkString{static_cast<std::size_t>(val), static_cast<std::size_t>(val)};
			}
			break;
		default:
			break;
		}

		return false;

	}
	bool on_resp_complete(Resp resp, Consumer& consumer)
	{
		while (!m_arrays.empty())
		{
			m_arrays.back().vals.push_back(std::move(resp));
			assert(m_arrays.back().vals.size() <= m_arrays.back().expected_size);

			// Not completed yet
			if (m_arrays.back().vals.size() < m_arrays.back().expected_size)
				return false;

			resp = Resp{RespArray{std::move(m_arrays.back().vals)}};
			m_arrays.pop_back();
		}
		consumer.consume(std::move(resp), {});
		return true;
	}
	bool parse_bulk_string(Consumer& consumer)
	{
		assert(m_bulk_str);
		bool use_sax = (consumer.use_sax() && m_arrays.empty());

		bool result_consumed{false};
//fmt::print("bulk remain {}/{}\n", m_bulk_str_remain, m_buffer.size());

		// We are looking at a bulk string now.
		// Either we keep it in the buffer, and it will be parsed as
		// an RespString, or we can call the consumer(void*, size_t) to
		// consume it directly to save some memory.
		if (m_bulk_str->remain > m_buffer.size() && use_sax)
		{
			// consume the data
			consumer.callback_sax(m_buffer.data(), m_bulk_str->size);
			m_bulk_str->remain -= m_buffer.size();
			m_buffer.consume(m_buffer.size());

			// last piece
			if (m_bulk_str->remain == 0)
			{
				result_consumed = true;
				m_bulk_str.reset();
			}
		}
		// Bulk string is parsed completely
		else if (m_bulk_str->remain <= m_buffer.size())
		{
			if (use_sax)
			{
				consumer.callback_sax(
					boost::beast::buffers_prefix(m_bulk_str->remain, m_buffer.data()), m_bulk_str->size
				);
				result_consumed = true;
			}
			else
			{
				result_consumed = on_resp_complete(Resp{buffers_to_string(
					boost::beast::buffers_prefix(m_bulk_str->remain, m_buffer.data())
				)}, consumer);
			}
			m_buffer.consume(m_bulk_str->remain);
			m_bulk_str.reset();
		}
		return result_consumed;
	}

	// Decide whether there is enough data in m_buffer to be consumed as a bulk string.
	// If we are not parsing a bulk string (i.e. m_bulk_str is empty), this function will still return
	// true if the buffer is not empty.
	bool bulk_string_ready_for_consume(Consumer& consumer) const
	{
		// For SAX consumer, we only need to wait until we have enough bytes (i.e. more than m_min_sax_size).
		// Otherwise, we need to wait until the whole bulk string to be complete (i.e. remain < m_buffer.size())
		return (consumer.use_sax() && m_buffer.size() >= m_min_sax_size) ||
			m_bulk_str.value_or(BulkString{}).remain <= m_buffer.size();
	}

private:
	boost::beast::multi_buffer m_buffer;
	struct ArrayContext
	{
		std::size_t expected_size{};
		std::vector<Resp> vals;
	};
	std::vector<ArrayContext> m_arrays;
	struct BulkString
	{
		std::size_t size{};
		std::size_t remain{};
	};
	std::optional<BulkString> m_bulk_str;
	std::size_t m_min_sax_size{};
};

class CommandBatch
{
public:
	CommandBatch() = default;

	template <
	    std::size_t N, typename OnComplete, typename ... Args,
		typename=std::enable_if<std::is_convertible_v<OnComplete, Consumer>>
	>
	void command(OnComplete&& comp, const char (&fmt)[N], Args && ... args)
	{
		return command(std::forward<OnComplete>(comp), CommandBuilder{fmt, std::forward<Args>(args)...});
	}

	template <std::size_t N, typename ... Args>
	void command(const char (&fmt)[N], Args && ... args)
	{
		return command(Consumer{}, CommandBuilder{fmt, std::forward<Args>(args)...});
	}
	void command(CommandBuilder&& cmd)
	{
		return command(Consumer{}, std::move(cmd));
	}

	template <
		typename OnComplete,
		typename=std::enable_if<std::is_convertible_v<OnComplete, Consumer>>
	>
	void command(OnComplete&& handler, CommandBuilder&& cmd)
	{
		m_handlers.emplace_back(std::forward<OnComplete>(handler));
		m_commands.append(std::move(cmd.buffers()));
	}
	void commands(CommandBatch&& batch)
	{
		for (auto& handler : batch.m_handlers)
			m_handlers.push_back(std::move(handler));
		m_commands.append(std::move(batch.m_commands));
		batch.m_handlers.clear();
		batch.m_commands.clear();
	}
	void bind_executor(const boost::asio::any_io_executor& exec)
	{
		for (auto& handler : m_handlers)
			handler.bind_executor(exec);
	}

	auto command_buffers() const {return m_commands.buffers();}
	auto has_command() const {return !m_commands.empty();}
	void clear_command() {m_commands.clear();}

	auto& front_handler() {return m_handlers.front();}
	void pop_handler() {m_handlers.pop_front();}
	auto has_handler() const {return !m_handlers.empty();}

	void clear(std::error_code ec)
	{
		for (auto&& c : m_handlers)
			c.consume({}, ec);
		m_handlers.clear();
		m_commands.clear();
	}

private:
	BufferList	m_commands;
	std::deque<Consumer>	m_handlers;
};

class Connection : public std::enable_shared_from_this<Connection>
{
public:
	template <typename Executor, typename=std::enable_if<boost::asio::execution::is_executor_v<Executor>>>
	explicit Connection(Executor oic) :
		m_exec{std::move(oic)}, m_socket{make_strand(m_exec)}
	{
	}

	template <typename Executor, typename=std::enable_if<boost::asio::execution::is_executor_v<Executor>>>
	Connection(Executor oic, boost::asio::ip::tcp::socket&& socket) :
		m_exec{std::move(oic)}, m_socket{std::move(socket)}
	{
	}

	Connection(Connection&&) = delete;
	Connection(const Connection&) = delete;
	~Connection() = default;

	Connection& operator=(Connection&&) = delete;
	Connection& operator=(const Connection&) = delete;

	void set_logger(std::function<void(const std::string&)> logger){m_logger = std::move(logger);}

	bool is_open() const {return m_socket.is_open();}
	void close()
	{
		m_resolver.cancel();
		m_socket.close();
	}

	template <typename Complete>
	void async_connect(std::string_view host, std::string_view port, Complete&& comp)
	{
		m_resolver.async_resolve(
			host, port,
			boost::asio::bind_executor(
				m_exec,
				[this, self=shared_from_this(), comp=std::forward<Complete>(comp)](auto ec, auto results) mutable
				{
					if (ec)
						comp(std::move(self), ec);
					else
						this->async_connect(std::move(results), std::forward<Complete>(comp));
				}
			)
		);
	}

	template <typename ListOfEndPoints, typename Complete>
	void async_connect(ListOfEndPoints&& endpoints, Complete&& comp)
	{
		boost::asio::async_connect(
			m_socket, endpoints,
			boost::asio::bind_executor(
				m_exec,
				[comp=std::forward<Complete>(comp), self=shared_from_this()](auto ec, auto&&) mutable
				{
					comp(std::move(self), ec);
				}
			)
		);
	}

	void connect(std::string_view host, std::string_view port, std::error_code& ec)
	{
		boost::system::error_code bec;
		auto result = m_resolver.resolve(host, port, bec);
		if (!bec)
			boost::asio::connect(m_socket, result, bec);

		ec = bec;
	}

	template <typename OnComplete, typename=std::enable_if<std::is_convertible_v<OnComplete, Consumer>>>
	void queue_command(OnComplete&& comp, CommandBuilder cmd)
	{
		m_pending.command(std::forward<OnComplete>(comp), std::move(cmd));
	}
	void queue_commands(CommandBatch&& batch)
	{
		m_pending.commands(std::move(batch));
	}
	void commands(CommandBatch&& batch)
	{
		queue_commands(std::move(batch));
		send_queued_commands();
	}
	template <typename OnComplete, typename=std::enable_if<std::is_convertible_v<OnComplete, Consumer>>>
	void command(OnComplete&& comp, CommandBuilder cmd)
	{
		queue_command(std::move(comp), std::move(cmd));
		send_queued_commands();
	}

	template <
	    std::size_t N, typename OnComplete, typename ... Args,
		typename=std::enable_if<std::is_convertible_v<OnComplete, Consumer>>
	>
	void queue_command(OnComplete&& comp, const char (&fmt)[N], Args && ... args)
	{
		return queue_command(std::forward<OnComplete>(comp), CommandBuilder{fmt, std::forward<Args>(args)...});
	}

	template <std::size_t N, typename OnComplete, typename ... Args>
	void command(OnComplete&& comp, const char (&fmt)[N], Args && ... args)
	{
		return command(std::forward<OnComplete>(comp), CommandBuilder{fmt, std::forward<Args>(args)...});
	}

	bool send_queued_commands()
	{
		return do_write();
	}

	auto get_executor() {return m_exec;}
	auto& set_read_buffer_size(std::size_t size)
	{
		m_read_buffer_size = size;
		m_reader.set_min_sax_size(size);
		return *this;
	}

private:
	// must not call disconnect() inside the callbacks in m_callbacks
	void try_disconnect(std::error_code ec)
	{
		if (m_socket.is_open())
		{
			m_logger(fmt::format("disconnecting connection to redis: {} ({})", ec, ec.message()));
			m_socket.close();
		}

		m_inflight.clear(ec);
		m_pending.clear(ec);
	}

	void try_parse()
	{
		// Be very careful here.
		// m_read.parse(m_inflight.handlers.front()) may call a handler.
		// This callback is supplied by the caller application, which may call
		// public functions of this class.
		// The state before parse() may not be consistent to the state after parse()!
		while (m_inflight.has_handler() && m_reader.parse(m_inflight.front_handler()))
			m_inflight.pop_handler();

		// Keep reading responses from redis server until all inflight commands have
		// been processed.
		if (is_inflight())
			do_read();

		// The response of all inflight commands has been received and processed.
		// We can send another batch of commands.
		else
			send_queued_commands();
	}

	bool is_inflight() const
	{
		return m_inflight.has_command() || m_inflight.has_handler();
	}

	void do_read()
	{
		assert(is_inflight());

		auto on_read = boost::asio::bind_executor(m_exec, [this, self=shared_from_this()](auto ec, auto read) mutable
		{
			if (!ec)
			{
				assert(is_inflight());
				m_reader.commit(read);
				try_parse();
			}
			else
			{
				m_logger(fmt::format("Read error: {} ({})", std::error_code{ec}, ec.message()));
				try_disconnect(ec);
			}
		});

		m_socket.async_read_some(m_reader.prepare(m_read_buffer_size), std::move(on_read));
	}

	bool do_write()
	{
		// After try_parse() successfully parse a response from redis server,
		// the top of m_inflight.handlers queue will be called.
		// This callback function may call queue_command() and do_write() may be called.
		// At this moment, the callback function is still on top of the m_inflight.handlers queue,
		// so m_inflight.handlers is not empty yet.
		// The callback function will be popped from m_inflight.handlers very soon.
		// As a result, we need to check both m_inflight.commands and m_inflight.handlers to be empty before
		// proceed writing.
		if (is_inflight())
			return false;

		// Prepare the buffers to be sent to redis server (i.e. m_writing).
		// m_callbacks will be the callbacks that handles the response from the server.
		// If m_inflight.commands contains 3 redis command, there will be 3 callbacks in m_inflight.handlers.
		// We won't send anything more to the server until we got the response of all 3 commands
		// we sent. The server may not respond correctly if we send something to it before we
		// hear it back.
		m_inflight = std::exchange(m_pending, {});
		assert(!m_pending.has_command());

		// Nothing to do.
		if (!m_inflight.has_command())
			return false;

		auto on_write = boost::asio::bind_executor(m_exec, [this, self=shared_from_this()](auto ec, auto) mutable
		{
//fmt::print("written {} bytes\n", bytes);
			m_inflight.clear_command();
			if (ec)
			{
				m_logger(fmt::format("Write error: {} ({})", std::error_code{ec}, ec.message()));
				try_disconnect(ec);
			}
			else
			{
				assert(is_inflight());
				do_read();
			}
		});

//fmt::print("writing {} {} bytes {} commands\n", boost::beast::buffers_to_string(boost::beast::buffers_prefix(20, m_inflight.commands.buffers())),
//fmt::print("writing {} {} bytes {} commands\n", boost::beast::buffers_to_string(m_inflight.commands.buffers()),
//	m_inflight.commands.total_size(), m_inflight.handlers.size());
		boost::asio::async_write(m_socket, m_inflight.command_buffers(), std::move(on_write));
		return true;
	}

private:
	boost::asio::any_io_executor    m_exec;
	boost::asio::ip::tcp::socket    m_socket;
	boost::asio::ip::tcp::resolver  m_resolver{m_socket.get_executor()};
	std::size_t m_read_buffer_size{8 * 1024};
	RespParser  m_reader{m_read_buffer_size};

	CommandBatch m_inflight, m_pending;
	std::function<void(const std::string&)> m_logger{[](auto&){}};
};

using Redis = std::shared_ptr<Connection>;

class RedisPool
{
public:
	RedisPool(
		std::string host, std::string port,
		std::function<void(const std::string&)> logger = [](auto&&){}
	) : m_host{std::move(host)}, m_port{std::move(port)}, m_logger{std::move(logger)}
	{
	}

	// "conn_exec" is the executor that runs the callback passed to Connection::command() and its cousins.
	// Do not confuse it with m_sock_exec, which runs the socket callbacks in the async read/write of
	// "Connection". These socket callback will always dispatch() to conn_exec.
	template <typename Executor, typename=std::enable_if<boost::asio::execution::is_executor_v<Executor>>>
	Redis alloc(Executor exec, std::error_code& ec) const
	{
		auto conn = new_connection(std::move(exec));
		conn->connect(m_host, m_port, ec);
		return conn;
	}

	template <
	    typename Executor, typename Callback,
		typename=std::enable_if<boost::asio::execution::is_executor_v<Executor>>
	>
	Redis alloc(Executor exec, Callback&& callback) const
	{
		auto conn = new_connection(exec);
		assert(conn);

		// Note that this handler is called with the "conn" executor.
		// Need to call our "comp" handler in "exec" executor.
		conn->async_connect(m_host, m_port, std::move(callback));

		// For cancelling connection asynchronously.
		return conn;
	}

private:
	template <typename Executor, typename=std::enable_if<boost::asio::execution::is_executor_v<Executor>>>
	Redis new_connection(Executor exec) const
	{
		auto conn = std::make_shared<Connection>(make_strand(std::move(exec)));
		conn->set_logger(m_logger);
		return conn;
	}

private:
	const std::string       m_host;
	const std::string       m_port;

	std::function<void(const std::string&)>     m_logger;
};

// fmt support
inline std::string format_as(const Resp& resp)
{
	return resp.visit(Overloaded{
		[](std::monostate)	        {return std::string{"<nil>"};},
		[](const sanali::RespArray& a)
		{
			return fmt::format("[{}]", fmt::join(a.val, ","));
		},
		[](auto&& others)           {return fmt::format("{}", others.val);}
	});
}

inline std::ostream& operator<<(std::ostream& os, const Resp& resp)
{
	fmt::print(os, "{}", resp);
	return os;
}

} // end of namespace sanali
