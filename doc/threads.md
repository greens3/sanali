# Threads and Executors

Sanali is thread-neutral. It does not create threads internally. It always uses the threads provided by its caller.

Sanali uses executors to read and write to its socket, as well as running complete callbacks (i.e. the first argument
to `command()`). If you use a "multi-thread" executor with Sanali, Sanali can use multiple threads. Although
this is well-supported, be very careful with multi-threading because it is easy to get it wrong.

A typical usage would be to use a thread pool to as an executor:

```C++
boost::asio::thread_pool threads;
Redis redis{"127.0.0.1", "6379", threads.get_executor()};

redis.command([](auto&& resp, auto ec)
{
	do_something();
}, "Set key value");

redis.command([](auto&& resp, auto ec)
{
	do_something_else();
}, "Set another_key another_value");
```

In the above example, a thread pool executor is passed to `sanali::Redis`. This executor will be used to
execute all callback functions of `command()`. In other words, both `do_something()` and `do_something_else()`
will be run by the thread pool executor. Since `boost::asio::thread_pool` will create threads dynamically,
there is a chance that `do_something()` and `do_something_else()` will be run in parallel by two threads!
Moreover, it may even be possible for `do_something_else()` to be run (and finish) before `do_something()`.
To avoid data races, proper synchronization are required. For example, you will have to use a mutex to
protect the shared data.

On the other hand, each `sanali::Redis` object represents one single connection to the redis server. Since you can't
send two redis commands in parallel within one connection, the two "SET" commands in the above example were
always sent sequentially. That means the responses were received by Sanali sequentially, in the same order
as the commands were sent. In other words, there is no performance reason to handle the results of these
two commands in parallel with multiple threads. If we use one thread to run the both callbacks, it will not
run slower than multiple threads.

*Strands* are designed for situations like these. We can pass a strand as the executor to `sanali::Redis`, such
that all callback submitted by Sanali will be executed sequentially by the thread pool.

```C++
boost::asio::thread_pool threads;
Redis redis{"127.0.0.1", "6379", make_strand(threads.get_executor())};
```

Now we can ensure `do_something()` and `do_something_else()` will always run sequentially. They may still be
scheduled to different threads in the thread pool, but they will _never_ run in parallel. Therefore, there is
no need to use any synchronization even if the two callbacks access the same data.

## Thread Safety

Is `sanali::Redis` thread-safe? Short answer: no.

Long answer: if you use `io_context.get_executor()` as the executor when constructing `sanali::Redis`, then
yes, you can call `Redis::command()` and other member functions from multiple threads safely. Of course,
the destructor is an exception. Even if the destructor is thread-safe, calling destructor will multiple
threads will result in a double-free undefined behaviour (most likely a crash).

It's generally a bad idea to share `sanali::Redis` objects among multiple threads. Use `RedisPool` instead.