//
// Created by nestal on 11/03/24.
//

#pragma once

#include "Sanali.hh"

#include <cstdlib>
#include <random>
#include <string_view>

class RedisFixture
{
public:
	static std::string redis_host()
	{
		auto env_host = std::getenv("SANALI_REDIS_HOST");
		return std::string{env_host ? env_host : "localhost"};
	}

	static std::string redis_port()
	{
		auto env_port = std::getenv("SANALI_REDIS_PORT");
		return std::string{env_port ? env_port : sanali::redis_port};
	}

	RedisFixture() : pool{redis_host(), redis_port(), [](auto&& str){fmt::print("{}\n", str);}}
	{
		std::error_code ec;
		conn = pool.alloc(ioc.get_executor(), ec);
		if (ec)
			fmt::print(stderr, "Cannot connect to redis: {} {}", ec, ec.message());
	}

	std::string randomize(std::size_t min_size, std::size_t max_size)
	{
		if (min_size > max_size)
			std::swap(min_size, max_size);
		std::string large_string;
		std::uniform_int_distribution<unsigned> data_dist{static_cast<unsigned>('a'), static_cast<unsigned>('z')};
		std::uniform_int_distribution<std::size_t> size_dist{min_size, max_size};

		auto rand_size = size_dist(m_gen);
		assert(rand_size <= max_size);
		std::generate_n(
			std::back_inserter(large_string),
			rand_size,
			[&]{return static_cast<char>(data_dist(m_gen));}
		);
		return large_string;
	}

protected:
	static std::random_device m_rd;
	std::mt19937 m_gen{m_rd()};

	boost::asio::io_context ioc;
	sanali::RedisPool pool;
	sanali::Redis conn;
};

template <typename Rep=std::chrono::seconds::rep, typename Period=std::chrono::seconds::period>
bool RunFor(
	bool& cond, boost::asio::io_context& ioc,
	std::chrono::duration<Rep, Period> duration = std::chrono::seconds{10}
)
{
	auto until = std::chrono::steady_clock::now() + duration;
	while (!cond && ioc.run_one_until(until));
	ioc.restart();
	return std::exchange(cond, false);
}
