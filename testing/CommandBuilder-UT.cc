/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the sanali
    distribution for more details.
*/

//
// Created by nestal on 7/19/23.
//

#include "Sanali.hh"

#include <catch2/catch_all.hpp>
#include <iostream>

using namespace sanali;

TEST_CASE("Excess number of arguments for BufferFormatter will be ignored", "[normal]")
{
	BufferFormatter subject{"3 replacement strings {} {} {}!"};
	subject.format(1, 3, 4, 5, 7, 8, "hello world!");
	REQUIRE(boost::beast::buffers_to_string(subject.storage().buffers()) == "3 replacement strings 1 3 4!");
}

TEST_CASE("multi_buffer will NOT be moved by try_format() without replacement string", "[normal]")
{
	BufferFormatter subject{"no replacement string"};
	boost::beast::multi_buffer mb;
	boost::asio::buffer_copy(mb.prepare(12), boost::asio::buffer("hello world!"));
	mb.commit(12);

	subject.try_format(std::move(mb));
	subject.commit();
	REQUIRE(mb.size() == 12);
	REQUIRE(boost::beast::buffers_to_string(mb.data()) == "hello world!");
	REQUIRE(subject.storage().size() == 1);
}

TEST_CASE("multi_buffer will be moved by try_format() if the field has replacement string", "[normal]")
{
	BufferFormatter subject{"__{}__"};
	boost::beast::multi_buffer mb;
	while (mb.size() < 4096)
	{
		boost::asio::buffer_copy(mb.prepare(12), boost::asio::buffer("hello world!"));
		mb.commit(12);
	}
	subject.try_format(std::move(mb));
	subject.commit();
	REQUIRE(mb.size() == 0);
	REQUIRE(subject.storage().size() == 3);
}

TEST_CASE("Shallow copy fields with std::ref", "[normal]")
{
	BufferFormatter subject{"__{}__{}__"};
	const char long_string[] = "hello world 1234567890 1234567890 1234567890";
	boost::asio::const_buffer cbuf{std::data(long_string), std::size(long_string)-1};
	subject.try_format(BufferRef{cbuf});
	subject.try_format(123.45);
	subject.commit();
	REQUIRE(boost::beast::buffers_to_string(subject.storage().buffers()) == "__hello world 1234567890 1234567890 1234567890__123.45__");
	REQUIRE(subject.storage().size() == 3);
}

TEST_CASE("Deep copy fields", "[normal]")
{
	BufferFormatter subject{"__{}__"};
	std::string_view long_string{"hello world 1234567890 1234567890 1234567890"};
	subject.try_format(long_string);
	subject.commit();
	REQUIRE(boost::beast::buffers_to_string(subject.storage().buffers()) == "__hello world 1234567890 1234567890 1234567890__");
	REQUIRE(subject.storage().size() == 1);
}

TEST_CASE("Field formatter formats binary fields", "[normal]")
{
	BufferFormatter subject{"(^{}^)"};
	std::array<unsigned char, 4> bytes{'\x10', '\x20', '\x30', '\x40'};
	subject.try_format(boost::asio::buffer(bytes));
	subject.commit();
	REQUIRE(boost::beast::buffers_to_string(subject.storage().buffers()) == "(^\x10\x20\x30\x40^)");
	REQUIRE(subject.storage().size() == 1);
}

TEST_CASE("Field formatter formats simple fields", "[normal]")
{
	BufferFormatter subject{"123{}456"};
	subject.try_format(3.4);
	subject.commit();
	REQUIRE(boost::beast::buffers_to_string(subject.storage().buffers()) == "1233.4456");

	auto subject2{format("123{}456", 5.6)};
	REQUIRE(boost::beast::buffers_to_string(subject2.buffers()) == "1235.6456");
}

TEST_CASE("Field formatter formats two fields", "[normal]")
{
	BufferFormatter subject{"{:x>6}15{}4345"};
	REQUIRE(subject.try_format(3.14));
	REQUIRE(subject.try_format(926));
	REQUIRE_FALSE(subject.try_format(nullptr));
	subject.commit();
	REQUIRE(boost::beast::buffers_to_string(subject.storage().buffers()) == "xx3.14159264345");
}

TEST_CASE("The first field must not be a replacement field", "[normal]")
{
	REQUIRE_THROWS(CommandBuilder{"{} key value", "SET"});
}

TEST_CASE("build command with buffer list as argument", "[normal]")
{
	auto buffer_list = format("this is an argument {}", 1.5);
	CommandBuilder subject{"SET key {}", std::move(buffer_list)};
	REQUIRE(subject.string_count() == 3);
	REQUIRE(subject.as_string() == "*3\r\n$3\r\nSET\r\n$3\r\nkey\r\n$23\r\nthis is an argument 1.5\r\n");

	// Append empty string argument
	subject.append();
	REQUIRE(subject.string_count() == 4);
	REQUIRE(
		subject.as_string() == "*4\r\n$3\r\nSET\r\n$3\r\nkey\r\n$23\r\nthis is an argument 1.5\r\n"
							   "$0\r\n\r\n"
	);
}

TEST_CASE("build command by buffer reference", "[normal]")
{
	std::string long_str(1000, ' ');

	CommandBuilder eval{"EVAL {} 4", BufferList{boost::asio::buffer(long_str)}};
	REQUIRE(eval.string_count() == 3);

	auto buffers = eval.buffers().buffers();
	REQUIRE(buffers.size() == 4);

	// buffers[0] is ArrayHeader
	REQUIRE(boost::beast::buffers_to_string(buffers[0]) == "*3\r\n");
	REQUIRE(boost::beast::buffers_to_string(buffers[1]) == "$4\r\nEVAL\r\n$1000\r\n");

	// BufferRef will not be copied because it's longer than 80 bytes.
	REQUIRE(buffers[2].data() == long_str.data());
	REQUIRE(buffers[2].size() == long_str.size());

	REQUIRE(boost::beast::buffers_to_string(buffers[3]) == "\r\n$1\r\n4\r\n");

}

TEST_CASE("build command by arguments", "[normal]")
{
	CommandBuilder hmset{"HMSET {} {}", "key0", 30};
	REQUIRE(hmset.string_count() == 3);
	hmset.append("key1").append("{}", "value1");
	hmset.append("{}", "key2").append("longer {} {}", 15, "value");
	REQUIRE(hmset.string_count() == 7);
	REQUIRE(boost::beast::buffer_bytes(hmset.buffers().buffers()) == hmset.buffers().total_size());

	REQUIRE(hmset.as_string() ==
		"*7\r\n"
		"$5\r\nHMSET\r\n"
		"$4\r\nkey0\r\n$2\r\n30\r\n"
		"$4\r\nkey1\r\n$6\r\nvalue1\r\n"
		"$4\r\nkey2\r\n$15\r\nlonger 15 value\r\n"
	);
}

TEST_CASE("Command with empty argument", "[normal]")
{
	CommandBuilder subject{"SET    key  {}", ""};
	REQUIRE(subject.string_count() == 3);
	REQUIRE(subject.as_string() ==
		"*3\r\n"
		"$3\r\nSET\r\n"
		"$3\r\nkey\r\n$0\r\n\r\n"
	);
}

TEST_CASE("Only one string with {{}}", "[normal]")
{
	CommandBuilder subject{"{{}}zzz"};
	auto fmt_result = fmt::format("{{}}zzz");

	auto result = subject.as_string();
	REQUIRE(    result == "*1\r\n$5\r\n{}zzz\r\n");
	REQUIRE(fmt_result ==             "{}zzz");
	REQUIRE(subject.string_count() == 1);

	REQUIRE(boost::beast::buffers_to_string(subject.buffers().buffers()) == result);
}

TEST_CASE("SET large_string ~{}{~{z}z}", "[normal]")
{
	std::string large_string{"~}}zz|~{{~z}"};
	CommandBuilder subject{"SET large_string {}", large_string};

	REQUIRE(subject.as_string() == "*3\r\n$3\r\nSET\r\n$12\r\nlarge_string\r\n$12\r\n~}}zz|~{{~z}\r\n");
	REQUIRE(subject.string_count() == 3);
}

TEST_CASE("parse 3-string command with 2 replacement group in the same argument", "[normal]")
{
	int eax{1};
	CommandBuilder subject{"mov {} abc{:>5}{}123", eax, "{{}}", 2};

	REQUIRE(subject.as_string() == "*3\r\n$3\r\nmov\r\n$1\r\n1\r\n$12\r\nabc {{}}2123\r\n");
	REQUIRE(subject.string_count() == 3);
}

TEST_CASE("Constexpr char string", "[normal]")
{
	const char arg[] = "EVAL some_script 0";
	CommandBuilder subject{arg};
	REQUIRE(subject.as_string() == "*3\r\n$4\r\nEVAL\r\n$11\r\nsome_script\r\n$1\r\n0\r\n");
	REQUIRE(subject.string_count() == 3);

	subject.append(BufferList{"argv1{}"});
	REQUIRE(subject.as_string() == "*4\r\n$4\r\nEVAL\r\n$11\r\nsome_script\r\n$1\r\n0\r\n$7\r\nargv1{}\r\n");
	REQUIRE(subject.string_count() == 4);

	subject.append(30.56);
	REQUIRE(subject.as_string() ==
		"*5\r\n"
		"$4\r\nEVAL\r\n"
		"$11\r\nsome_script\r\n"
		"$1\r\n0\r\n"
		"$7\r\nargv1{}\r\n"
		"$5\r\n30.56\r\n"
	);
	REQUIRE(subject.string_count() == 5);

	subject.append("some {} string", 14.56);
	REQUIRE(subject.as_string() ==
		"*6\r\n"
		"$4\r\nEVAL\r\n"
		"$11\r\nsome_script\r\n"
		"$1\r\n0\r\n"
		"$7\r\nargv1{}\r\n"
		"$5\r\n30.56\r\n"
		"$17\r\nsome 14.56 string\r\n"
	);
	REQUIRE(subject.string_count() == 6);
}

TEST_CASE("Buffer Ref", "[normal]")
{
	constexpr const char lua[] = "This is a lua script";

	CommandBuilder subject{"EVAL {} 3", buffer_ref(lua)};
	REQUIRE(subject.as_string() == "*3\r\n$4\r\nEVAL\r\n$20\r\nThis is a lua script\r\n$1\r\n3\r\n");
	REQUIRE(subject.string_count() == 3);

	subject.append(BufferList{"key1"});
	REQUIRE(subject.as_string() == "*4\r\n$4\r\nEVAL\r\n$20\r\nThis is a lua script\r\n$1\r\n3\r\n$4\r\nkey1\r\n");
	REQUIRE(subject.string_count() == 4);
}

TEST_CASE("KEY star", "[normal]")
{
	CommandBuilder subject{"KEY *"};
	REQUIRE(subject.as_string() == "*2\r\n$3\r\nKEY\r\n$1\r\n*\r\n");
	REQUIRE(subject.string_count() == 2);
}

TEST_CASE("parse command with { in replacement group", "[normal]")
{
	CommandBuilder subject{"mov {{}}{} {}{{}}", 15, 45.6};

	REQUIRE(subject.as_string() == "*3\r\n$3\r\nmov\r\n$4\r\n{}15\r\n$6\r\n45.6{}\r\n");
	REQUIRE(subject.string_count() == 3);
}

static_assert(extract_replacement_field("abc{}123").pos == 5);

TEST_CASE("replacement string without {}", "[normal]")
{
	std::string_view subject1{"abc{}{}edf"};
	{
		auto [rs, found] = extract_replacement_field(subject1);
		REQUIRE(subject1.substr(0, rs) == "abc{}");
		REQUIRE(found);
		subject1.remove_prefix(rs);
		REQUIRE(subject1 == "{}edf");
	}

	{
		auto [rs, found] = extract_replacement_field(subject1);
		REQUIRE(subject1.substr(0, rs) == "{}");
		REQUIRE(found);
		subject1.remove_prefix(rs);
		REQUIRE(subject1 == "edf");
	}
	{
		auto [rs, found] = extract_replacement_field(subject1);
		REQUIRE(subject1.substr(0,rs) == "edf");
		subject1.remove_prefix(rs);
		REQUIRE_FALSE(found);
	}
}

TEST_CASE("replacement string with {}", "[normal]")
{
	std::string_view subject1{"abc{{}}{}{}edf"};
	auto [rs, found] = extract_replacement_field(subject1);
	REQUIRE(subject1.substr(0,rs) == "abc{{}}{}");
	REQUIRE(found);
	subject1.remove_prefix(rs);
	REQUIRE(subject1 == "{}edf");
}
