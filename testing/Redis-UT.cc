/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the sanali
    distribution for more details.
*/

//
// Created by nestal on 7/21/23.
//

#include "Sanali.hh"
#include "Fixture.hh"

#include <fmt/chrono.h>
#include <catch2/catch_all.hpp>

#include <algorithm>
#include <random>

using namespace sanali;
using namespace std::chrono_literals;

std::random_device RedisFixture::m_rd;

TEST_CASE_METHOD(RedisFixture, "queuing transaction commands", "[normal]")
{
	auto checker = [](auto&& resp, auto ec)
	{
		INFO("resp = " << resp);
		REQUIRE(!resp.is_error());
		REQUIRE_FALSE(ec);
	};

	int key = std::uniform_int_distribution<int>{0, 250}(m_gen);
	bool checked{false};

	REQUIRE(conn->is_open());

	conn->queue_command(checker, "MULTI");
	conn->queue_command(checker, "DEL somekey_{}", key);
	conn->queue_command(checker, "SET somekey_{} some_value_{}", key, key);
//	REQUIRE(conn->inflight_count() == 0);
//	REQUIRE(conn->pending_count() == 3);

	conn->command([key, this, &checked](auto&& resp, auto ec)
	{
		REQUIRE(!resp.is_nil());
		REQUIRE(!resp.is_error());
		REQUIRE_FALSE(ec);

		conn->command([key, &checked](auto&& resp, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(resp == fmt::format("some_value_{}", key));
			checked = true;
		}, "GET somekey_{}", key);
	}, "EXEC");
	ioc.run_for(10s);
	REQUIRE(checked);
}

TEST_CASE_METHOD(RedisFixture, "variable number of argument in lua script", "[normal]")
{
	CommandBuilder script{
		"EVAL {} 2 key{} hash{} {}",
		R"__(
		redis.call("SET", KEYS[1], ARGV[1])
		table.remove(ARGV, 1)
		return redis.call("HSET", KEYS[2], unpack(ARGV))
		)__",
		1.5, 1.5, "this is argv1"
	};

	std::uniform_int_distribution<int> rand_count{10, 20};
	auto field_count{rand_count(m_gen)};

	// argument for HSET
	std::vector<std::string> values;
	for (int i=0; i< field_count; ++i)
	{
		values.push_back(randomize(20, 30));
		script.append("field{}", i).append("a very long value: {}", values.back());
	}

	bool checked{false};
	conn->command([&checked, this, field_count, &values](auto&& added, std::error_code ec)
	{
		REQUIRE_FALSE(ec);
		INFO("added = " << added);

		// read the value back from redis
		auto index = std::uniform_int_distribution<int>{0, field_count-1}(m_gen);
		conn->command([&checked, &values, index](auto&& value, std::error_code ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(value == "a very long value: " + values.at(index));
			checked = true;

		}, "HGET hash{} field{}", 1.5, index);

	}, std::move(script));
	ioc.run_for(10s);
	REQUIRE(checked);
}

TEST_CASE_METHOD(RedisFixture, "HSET multiple key values" "[normal]")
{
	CommandBuilder hset{"HSET some_hash"};
	hset.append("{}", "\"key1\"").append("{}", "value1");
	hset.append("{}", "key2").append("{}xxx{}", randomize(10, 20), randomize(sizeof(std::string), 30));

	// append more fields
	hset.append("{}", "\"11key3\"").append("{}", "value1");
	hset.append("{}", "key5").append("{}", randomize(30, 40));

	bool checked{false};
	conn->command([&checked](auto&& added, std::error_code ec)
	{
		REQUIRE_FALSE(ec);
		REQUIRE(added.to_int() <= 4);
		checked = true;
	}, std::move(hset));
	ioc.run_for(10s);
	REQUIRE(checked);
}

TEST_CASE_METHOD(RedisFixture, "storing raw bytes as binary", "[normal]")
{
	int checks{};

	std::array<unsigned char, 4> bytes{'\x10', '\x20', '\x30', '\x40'};
	conn->command([&checks, this](auto&& result, auto ec)
	{
		REQUIRE(result.ok());
		REQUIRE_FALSE(ec);
		conn->command([&checks](auto&& result, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(result == "(^\x10\x20\x30\x40^)");
			++checks;
		}, "GET bin_key");
	}, "SET bin_key (^{}^)", boost::asio::buffer(bytes));

	ioc.run_for(10s);
	REQUIRE(checks == 1);
}

TEST_CASE("redis server not started", "[error]")
{
	auto c = std::make_shared<Connection>(boost::asio::system_executor{});

	std::error_code ec;
	c->connect("127.0.0.1", "1", ec);
	REQUIRE(ec); // assume no one listen to this port
}

TEST_CASE_METHOD(RedisFixture, "synchronous usage", "[normal]")
{
	// For sync_command() to work, some other thread has to call ioc.run()
//	auto worker = boost::asio::make_work_guard(ioc);
//	std::thread thread{[this]{ioc.run();}};
//
//	REQUIRE(conn->sync_command("SET user:{} {}", "user!?$", "this is a user").get().ok());
//	REQUIRE(conn->sync_command("GET user:{}", "user!?$").get() == Resp{"this is a user"});
//	REQUIRE(conn->sync_command("DEL user:{}", "user!?$").get() == Resp{1});
//	REQUIRE(conn->sync_command("GET user:{}", "user!?$").get() == Resp{});
//
//	worker.reset();
//	thread.join();
}
/*
TEST_CASE("synchronous usage using system executor", "[normal]")
{
	Redis conn{RedisFixture::redis_host(), RedisFixture::redis_port()};

	auto fut1 = conn.sync_command("SET {} value_is_{}", "some pi", 3.14);
	auto fut2 = conn.sync_command("GET {}",             "some pi");

	// Get them in reverse order
	REQUIRE(fut2.get() == Resp{"value_is_3.14"});
	REQUIRE(fut1.get().ok());
}

TEST_CASE("asynchronous usage using system executor", "[normal]")
{
	Redis conn{RedisFixture::redis_host(), RedisFixture::redis_port()};

	std::promise<std::string> promise;
	auto date = promise.get_future();

	conn.command([date=std::move(promise), &conn](auto&& resp, auto ec) mutable
	{
		if (ec || !resp.ok())
			date.set_exception(std::make_exception_ptr(std::system_error(ec)));

		else
			conn.command([date=std::move(date)](auto&& resp, auto ec) mutable
			{
				if (ec || !resp.is_string())
					date.set_exception(std::make_exception_ptr(std::system_error(ec)));

				else
					date.set_value(resp.as_string());
			}, "GET some_{}", "date");
	}, "SET some_date {}-{}-{}", 1989, "May", 35);

	REQUIRE(date.get() == "1989-May-35");
//	Redis::SystemExecutor{}.context().join();
}
*/
TEST_CASE_METHOD(RedisFixture, "use boost::beast::multi_buffer to avoid copying", "[normal]")
{
	boost::beast::multi_buffer bmi;

	for (int i = 0; i < 3; ++i)
	{
		auto rand1 = randomize(4096, 8192);
		auto buf1 = bmi.prepare(rand1.size());
		boost::asio::buffer_copy(buf1, boost::asio::buffer(rand1));
		bmi.commit(boost::asio::buffer_size(buf1));
	}

	int checks{};
	conn->command([&checks](auto&& result, auto ec)
	{
		REQUIRE(result.ok());
		REQUIRE(!ec);
		++checks;

	}, "SET multi_key {}__{}", std::move(bmi), 13);

	ioc.run_for(10s);
	REQUIRE(checks == 1);
}

TEST_CASE_METHOD(RedisFixture, "use boost::const_buffer to avoid copying", "[normal]")
{
	auto large_string = randomize(4096, 8192);

	int checks{};
	conn->command([&checks, &large_string, this](auto&& result, auto ec)
	{
		REQUIRE(result.ok());
		REQUIRE(!ec);
		++checks;

		conn->command([&checks, &large_string](auto&& result, auto ec)
		{
			REQUIRE(result == ("--" + large_string + "--"));
			REQUIRE_FALSE(ec);
			++checks;

		}, "GET bufkey");

	}, "SET bufkey --{}--", boost::asio::buffer(large_string));
	ioc.run_for(10s);
	REQUIRE(checks == 2);
}

TEST_CASE_METHOD(RedisFixture, "use fmt to format redis commands", "[normal]")
{
	int checks{};
	conn->command([&](auto&& result, auto ec)
	{
		REQUIRE(result.ok());
		REQUIRE(!ec);
		++checks;
	}, "SET now_str time-is-{}", std::chrono::system_clock::now());
	ioc.run_for(10s);
	REQUIRE(checks == 1);
}

TEST_CASE_METHOD(RedisFixture, "overlapped redis commands", "[normal]")
{
	int checks{};
	auto some_value = getpid();

	// Use a small buffer to make it easier to reproduce problems
	conn->set_read_buffer_size(80);

	conn->command([&](auto&& result, auto ec)
	{
		REQUIRE_FALSE(ec);
		REQUIRE(result.is_string());
		REQUIRE(result.ok());
		++checks;

		conn->command([&](auto&& result, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(result == std::to_string(some_value));
			++checks;
		}, "GET myid");

		conn->command([&](auto&& result, auto ec)
		{
			REQUIRE_FALSE(ec);
			auto& array = result.as_array();
			REQUIRE(std::find(array.begin(), array.end(), Resp{"myid"}) != array.end());
			++checks;
		}, "KEYS myid");

		// SAX interface
		auto s = std::make_shared<std::string>();
		conn->command([s, &checks, some_value](void* data, std::size_t size, auto total, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(data);
			REQUIRE(size > 0);
			s->append(static_cast<char*>(data), static_cast<char*>(data)+size);

			if (s->size() == total)
			{
				REQUIRE(*s == std::to_string(some_value));
				++checks;
			}
		}, "GET myid");

	}, "SET myid {}", some_value);

	ioc.run_for(10s);
	ioc.restart();
	REQUIRE(checks == 4);
	checks = 0;

	// delete the key we just added
	conn->command([&](auto&& removed, auto ec)
	{
		REQUIRE_FALSE(ec);
		REQUIRE(removed.to_int() == 1);
		++checks;
	}, "DEL myid");

	ioc.run_for(10s);
	ioc.restart();
	REQUIRE(checks == 1);
	checks = 0;
}

TEST_CASE_METHOD(RedisFixture, "20MB randomize binary string test", "[normal]")
{
	int checks{};

	// 20MB string with random data
	std::string large_string = randomize(15*1024*1024, 20*1024*1024);
	conn->set_read_buffer_size(1024*1024);

	conn->command([&checks, &large_string, this](auto&& result, auto ec)
	{
//		REQUIRE(conn->inflight_count() == 1);
		REQUIRE_FALSE(ec);
		REQUIRE(result.ok());
		conn->command([&checks, &large_string, offset=std::make_shared<std::size_t>()](void* data, std::size_t size, auto total, auto ec)
		{
//			REQUIRE(conn->inflight_count() == 2);
//fmt::print("get {} {}/{} {} bytes\n", size, *offset, total, large_string.size());
			REQUIRE_FALSE(ec);
			REQUIRE(data);
			REQUIRE(size > 0);
			REQUIRE(size + *offset <= large_string.size());
			REQUIRE(std::memcmp(data, &large_string[*offset], size) == 0);
			*offset += size;
			if (*offset == total)
				++checks;
		}, "GET large_string");

		conn->command([this, &checks](auto&& resp, auto ec)
		{
//			REQUIRE(conn->inflight_count() == 1);
			REQUIRE_FALSE(ec);
			REQUIRE_FALSE(resp.is_error());

			// Command should be executed in sequence. This command will not finish before
			// the previous one.
			REQUIRE(checks == 1);

			conn->command([&checks](auto&& count, auto ec)
			{
//				REQUIRE(conn.inflight_count() == 1);
				REQUIRE_FALSE(ec);
				REQUIRE(count.as_int() == 1);
				++checks;
			}, "HDEL large_hash copy_of_large_string");
//			REQUIRE(conn.inflight_count() == 1);
//			REQUIRE(conn.pending_count() == 1);

		}, "HSET large_hash copy_of_large_string {}", BufferRef{boost::asio::buffer(large_string)});
//		REQUIRE(conn.pending_count() == 2);

	}, "SET large_string {}", BufferRef{boost::asio::buffer(large_string)});

	ioc.run_for(60s);
	ioc.restart();
	REQUIRE(checks == 2);
	checks = 0;

	// copy keys with multi_buffer
	conn->command([&checks, this](boost::beast::multi_buffer&& buffer, auto ec) mutable
	{
		REQUIRE_FALSE(ec);
		conn->command([&checks](auto&& resp, auto ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(resp.ok());
			++checks;
		}, "SET copy_using_bmi {}", std::move(buffer));
		REQUIRE(buffer.size() == 0);

	}, "GET large_string");

	ioc.run_for(20s);
	ioc.restart();
	REQUIRE(checks == 1);
	checks = 0;
}

TEST_CASE_METHOD(RedisFixture, "simple redis", "[normal]")
{
	auto tested = 0;

	conn->command([&tested](auto, auto) {tested++;}, "SET key {}", 100);

	SECTION("test sequential")
	{
		conn->command(
			[this, &tested](auto reply, auto&& ec)
			{
				REQUIRE_FALSE(ec);

				// Check if redis executes our commands sequentially
				REQUIRE(tested++ == 1);

				REQUIRE(reply == "100");

				conn->command(
					[&tested](auto reply, auto)
					{
						REQUIRE(reply);
						REQUIRE(tested++ == 2);
						REQUIRE(reply.as_int() == 1);
					}, "DEL key"
				);
			}, "GET key"
		);

		using namespace std::chrono_literals;
		REQUIRE(ioc.run_for(10s) > 0);
		REQUIRE(tested == 3);
	}
	SECTION("test array as map")
	{
		conn->command([this, &tested](auto&&, auto&& ec)
		{
			REQUIRE_FALSE(ec);
			REQUIRE(tested++ == 1);

			conn->command([&tested](auto&& reply, auto&& ec)
			{
				REQUIRE(!ec);
				REQUIRE(tested++ == 2);

				SECTION("verify map_kv_pair()")
				{
					auto [v1, v2] = reply.map_kv_pair("field1", "field2");
					REQUIRE(v1 == "value1");
					REQUIRE(v2 == "value2");
				}
				SECTION("verify as_tuple()")
				{
					std::error_code ec2;
					auto [n1, v1, n2, v2] = reply.template as_tuple<4>(ec2);
					REQUIRE(n1 == "field1");
					REQUIRE(v1 == "value1");
					REQUIRE(n2 == "field2");
					REQUIRE(v2 == "value2");
				}
				SECTION("verify begin/end")
				{
					std::vector<std::string> result;
					for (auto&& v : reply)
						result.emplace_back(v.as_string());

					REQUIRE(result[0] == "field1");
					REQUIRE(result[1] == "value1");
					REQUIRE(result[2] == "field2");
					REQUIRE(result[3] == "value2");
				}
				SECTION("verify kv_pairs() iterators")
				{
					auto index = 0;
					for (auto&& kv : reply.kv_pairs())
					{
						auto field = (index == 0 ? "field1" : "field2");
						auto value = (index == 0 ? "value1" : "value2");
						REQUIRE(kv.key() == field);
						REQUIRE(kv.value() == value);
						index++;
					}
					REQUIRE(index == 2);

					auto kvs = boost::copy_range<std::vector<Resp::KeyValue>>(reply.kv_pairs());
					REQUIRE(kvs.size() == 2);
					REQUIRE(kvs.front().key() == "field1");
					REQUIRE(kvs.front().value() == "value1");
					REQUIRE(kvs.back().key() == "field2");
					REQUIRE(kvs.back().value() == "value2");
				}
			}, "HGETALL test_hash");
		}, "HSET test_hash field1 value1 field2 value2");

		using namespace std::chrono_literals;
		REQUIRE(ioc.run_for(10s) > 0);
		REQUIRE(tested == 3);
	}
}
