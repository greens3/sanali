/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the sanali
    distribution for more details.
*/

//
// Created by nestal on 7/18/23.
//

#include "Sanali.hh"
#include <catch2/catch_all.hpp>
#include <fmt/ostream.h>

using namespace sanali;

TEST_CASE("basic Resp test", "[normal]")
{
	REQUIRE(Resp{}.is_nil());
	REQUIRE(Resp{100}.is_int());
	REQUIRE(Resp{"string"}.is_string());
	REQUIRE(Resp{RespError{"error"}}.is_error());
	REQUIRE_FALSE(Resp{});
	REQUIRE(Resp{} == Resp::nil());

	// Comparisons
	REQUIRE(Resp{"some str"} == "some str");
	REQUIRE("something else" == Resp{"something else"});
	REQUIRE(Resp{3001} == 3001);
	REQUIRE(1234 != Resp{2345});
	REQUIRE("text" != Resp{23});
	REQUIRE("1000" == Resp{"1000"});

	Resp array{RespArray{}};
	array.as_array().emplace_back(1);
	array.as_array().emplace_back(2);
	array.as_array().emplace_back("something");
	array.as_array().emplace_back(RespError{"-error"});
	REQUIRE(fmt::to_string(array) == "[1,2,something,-error]");

	// mutable strings
	Resp srsp{"string"};
	REQUIRE(srsp == "string");
	srsp.as_string().append("_view");
	REQUIRE(srsp == "string_view");
	REQUIRE(srsp.as_string("basic_string") == "string_view");
	REQUIRE(srsp.as_int({}) == 0);

	// Integer conversions
	REQUIRE(Resp{"5000"}.to_int() == 5000);
	REQUIRE(Resp{1001}.as_string("1") == "1");
	REQUIRE(Resp{1001}.as_string({}) == "");
	REQUIRE(Resp{1001}.to_string() == "1001");
	REQUIRE_THROWS(Resp{1002}.as_string());
	REQUIRE(Resp{"1.3e3"}.to_int() == 1300);
	REQUIRE(Resp{"5.12345e3"}.to_int() == 5123);

	std::error_code ec{};
	REQUIRE(Resp{"8.00001e3"}.to_int(ec) == 8000);
	REQUIRE_FALSE(ec);
	REQUIRE(Resp{"8.00001e3___"}.to_int(ec) == 8000);
	REQUIRE_FALSE(ec);
	REQUIRE(Resp{"1712268632479603677"}.to_int(ec) == 1712268632479603677);
	REQUIRE_FALSE(ec);

	// Double conversions
	REQUIRE(Resp{"3.14159265358"}.to_double() == 3.14159265358);
	REQUIRE(Resp{"3.1415926"}.from_chars<double>(ec) == 3.1415926);
	REQUIRE_FALSE(ec);
	REQUIRE(Resp{"4.005001e3___"}.to_double(ec) == 4005.001);
	REQUIRE_FALSE(ec);

	REQUIRE(Resp{"not_a_string"}.from_chars(ec, 1001) == 1001);
	REQUIRE(ec == std::errc::invalid_argument);
	REQUIRE(Resp{"not_a_string"}.from_chars(ec, 1001.3) == 1001.3);
	REQUIRE(ec == std::errc::invalid_argument);
	REQUIRE(Resp{RespError{"-command error"}}.from_chars(ec) == 0);
	REQUIRE(ec == Error::incorrect_type);
	REQUIRE(Resp{}.to_int(3018) == 3018);
	REQUIRE(array.to_int(2012) == 2012);
	REQUIRE(Resp{"hello"}.to_double(ec) == 0.0);
	REQUIRE(ec == std::errc::invalid_argument);

	// ok() is case-sensitive
	REQUIRE(Resp{"OK"}.ok());
	REQUIRE_FALSE(Resp{"Ok"}.ok());
}

TEST_CASE("test Resp array", "[normal]")
{
	Resp array{RespArray{}};
	array.as_array().emplace_back("1st");
	array.as_array().emplace_back("2nd");
	array.as_array().emplace_back("3rd");
	array.as_array().emplace_back("4th");
	REQUIRE(array.is_array());

	REQUIRE(array.array_size() == 4);
	REQUIRE(array.as_array(0) == "1st");
	REQUIRE(array.as_array(3) == "4th");

	std::error_code error;
	REQUIRE(array.as_tuple<2>(error) == std::make_tuple(Resp{"1st"}, Resp{"2nd"}));
	REQUIRE_FALSE(error);

	std::size_t index = 0;
	for (auto&& kv : array.kv_pairs())
	{
		if (index == 0)
		{
			REQUIRE(kv.key() == "1st");
			REQUIRE(kv.value() == "2nd");
		}
		else
		{
			assert(index == 1);
			REQUIRE(kv.key() == "3rd");
			REQUIRE(kv.value() == "4th");
		}
		++index;
	}
	REQUIRE(index == 2);
}

TEST_CASE("test bulk string with SAX", "[normal]")
{
	RespParser subject;
	subject.push(std::string_view{"$12\r\nhello world!\r\n"});

	std::string out;
	Consumer consumer{[&](void* data, std::size_t size, auto total, auto ec)
	{
		REQUIRE_FALSE(ec);
		REQUIRE(size > 0);
		REQUIRE(data != 0);
		REQUIRE(total == 12);
		out.append(reinterpret_cast<char*>(data), size);
	}};
	REQUIRE(subject.parse(consumer));
	REQUIRE(out == "hello world!");
}

TEST_CASE("test array with zero-length string", "[normal]")
{
	RespParser subject;
	subject.push(std::string_view{"*3\r\n$0\r\n\r\n$1\r\na\r\n$0\r\n\r\n"});
	auto resp = subject.parse();
	REQUIRE(resp);

	using namespace std::literals;
	REQUIRE(resp->is_array());
	REQUIRE(resp->array_size() == 3);
	REQUIRE(resp->as_array(0) == "");
	REQUIRE(resp->as_array(1) == "a"s);
	REQUIRE(resp->as_array(2) == "");
}

TEST_CASE("parser test for debugging", "[normal]")
{
	RespParser subject;
	subject.push("*-1\r\n");
	REQUIRE(subject.parse() == Resp{});
}

TEST_CASE("incremental parsing of large bulk strings", "[normal]")
{
	RespParser subject;
	subject.push("$100");
	REQUIRE(subject.parse() == std::nullopt);
	subject.push("\r\n");
	REQUIRE(subject.parse() == std::nullopt);
	subject.push(std::string(50, 'a'));
	REQUIRE(subject.parse() == std::nullopt);
	subject.push(std::string(50, 'b'));
	REQUIRE(subject.parse() == Resp{std::string(50, 'a') + std::string(50, 'b')});
	subject.push("\r\n");
	REQUIRE(subject.parse() == std::nullopt);

	// Next one
	subject.push("*-1\r\n-");
	REQUIRE(subject.parse() == Resp{});
	REQUIRE(subject.parse() == std::nullopt);
	subject.push("ERR\r\n");
	REQUIRE(subject.parse().value() == Resp{RespError{"ERR"}});
}

TEST_CASE("data driven parsing tests", "[normal]")
{
	struct Case
	{
		std::string input;
		Resp result;
		std::error_code ec;
	};

	using namespace std::literals;
	auto c = GENERATE(
		Case{"+OK\r\n", Resp{"OK"s}, {}},
		Case{"+no_SO_OK\r\n", Resp{"no_SO_OK"s}, {}},
		Case{"$0\r\n\r\n", Resp{""s}, {}},
		Case{"$4\r\nABCD\r\n", Resp{"ABCD"s}, {}},
		Case{"$6\r\n12\r\n34\r\n", Resp{"12\r\n34"s}, {}},

		// bulk string with residual data before newline
		Case{"$4\r\n0123456\r\n", Resp{"0123"s}, {}},

		Case{"*0\r\n", Resp{RespArray{}}, {}},

		Case{":8964\r\n",       Resp{8964l}, {}},
		Case{":-19890604\r\n",  Resp{-19890604l}, {}},
		Case{":-1\r\n",   Resp{-1l}, {}},
		Case{"$-1\r\n",   Resp{}, {}},
		Case{"*-1\r\n",   Resp{}, {}},

		Case{"-Some error\r\n", Resp{RespError{"Some error"}}, sanali::Error::command_error},
		Case{"-\r\n",           Resp{RespError{""}},           sanali::Error::command_error}
	);

	INFO("Test case: " << c.input << " " << fmt::format("'{}'", c.result));
	RespParser subject;
	subject.push(c.input);

	bool checked{};
	Consumer consumer{[&](auto&& out, auto ec)
	{
		REQUIRE(c.result == out);
		REQUIRE_FALSE(c.result != out);
		REQUIRE(c.ec == ec);
		checked = true;
	}};
	REQUIRE(subject.parse(consumer));
	REQUIRE(checked);
}
