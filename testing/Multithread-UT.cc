/*
	Copyright © 2023 Wan Wai Ho <me@nestal.net>
    
    This file is subject to the terms and conditions of the GNU General Public
    License.  See the file COPYING in the main directory of the sanali
    distribution for more details.
*/

//
// Created by nestal on 8/27/23.
//

#include <catch2/catch_all.hpp>
#include "Sanali.hh"
#include "Fixture.hh"

#include <future>
#include <thread>

#include <boost/asio/thread_pool.hpp>
#include <boost/asio/basic_waitable_timer.hpp>

using namespace sanali;
using namespace std::chrono_literals;

TEST_CASE("multi-thread with pool", "[normal]")
{
	boost::asio::thread_pool threads;
	RedisPool pool{RedisFixture::redis_host(), RedisFixture::redis_port()};

	std::error_code ec;
	auto redis = pool.alloc(threads.get_executor(), ec);
	REQUIRE_FALSE(ec);

	std::promise<void> promise;
	auto done = promise.get_future();
	redis->command([done=std::move(promise)](auto&& resp, auto ec) mutable
	{
		REQUIRE(resp.ok());
		REQUIRE_FALSE(ec);
		done.set_value();
	}, "SET key {}", 230);
	REQUIRE(done.wait_for(30s) == std::future_status::ready);

	// Open another redis connection to get the result
	auto redis2 = pool.alloc(threads.get_executor(), ec);
	REQUIRE_FALSE(ec);

	std::promise<void> promise2;
	auto done2 = promise2.get_future();
	redis2->command([done=std::move(promise2)](auto&& resp, auto ec) mutable
	{
		REQUIRE_FALSE(ec);
		REQUIRE(resp.to_int() == 230);
		done.set_value();
	}, "GET key");
	REQUIRE(done2.wait_for(30s) == std::future_status::ready);

	threads.stop();
	threads.join();
}

TEST_CASE("multi-thread with pool case 2", "[normal]")
{
	// Thread pool to run the RedisPool (i.e. the socket executor m_sock_exec)
	boost::asio::thread_pool pool_threads;
	RedisPool pool{RedisFixture::redis_host(), RedisFixture::redis_port()};

	// Thread pool to run the connections proxies (i.e. "redis_exec") are a distinct thread pool
	// from the socket executor thread pool. This make sure the sockets will be passed
	// between different threads.
	boost::asio::thread_pool worker_threads;

	std::condition_variable cond;
	std::mutex mutex;
	std::atomic_size_t checked{}, errors{}, assertions{};

	constexpr std::size_t count = 10'000;
	auto on_error = [&]{++errors; if (checked+errors==count)cond.notify_one();};

	auto test_get_key = [&](unsigned value)
	{
		pool.alloc(worker_threads.get_executor(), [&, value](auto redis, auto ec)
		{
			if (ec)
				return on_error();

			redis->command([&, value, redis](auto&& resp, auto ec)
			{
				if (ec)
					return on_error();

				if (resp.to_string() != fmt::format("value{}", value))
					++assertions;

				++checked;
				if (checked+errors==count)
					cond.notify_one();

			}, "GET key{}", value);
		});
	};

	auto test_set_key = [&](int value)
	{
		pool.alloc(worker_threads.get_executor(), [&, value](auto redis, auto ec)
		{
			if (ec)
				return on_error();

			CommandBatch transaction;
			transaction.command("MULTI");
			transaction.command("SET key{} some_other_value{}", value, value);
			transaction.command("SET key{} value{}", value, value);
//			transaction.command(CommandBuilder{"HSET hash_key{}", value}.append("field").append("{}", value));
			transaction.command([&, value](auto&& resp, auto ec)
			{
				if (ec || !resp.is_array())
					return on_error();

				for (auto& r : resp)
					if (!r.ok() && r.to_int() != 0)
						return on_error();

				test_get_key(value);
			}, "EXEC");
			redis->commands(std::move(transaction));
		});
	};

	for (unsigned value=0; value<count; ++value)
		post(worker_threads.get_executor(), [value, &test_set_key]{test_set_key(value);});

	std::unique_lock lk{mutex};
	cond.wait_for(lk, 120s, [&]{return checked+errors == count;});
	lk.unlock();

	worker_threads.stop();
	worker_threads.join();

	pool_threads.stop();
	pool_threads.join();

	fmt::print("success = {}, failed = {}\n", checked.load(), errors.load());

	REQUIRE(checked+errors == count);
	REQUIRE(checked > 0);
	REQUIRE(assertions == 0);
}

TEST_CASE("Always fail to connect", "[error]")
{
	boost::asio::thread_pool threads;
	RedisPool subject{"10.255.255.1", "6379"};

	std::promise<void> promise;
	auto done = promise.get_future();
	auto conn = subject.alloc(threads.get_executor(), [done=std::move(promise)](auto, auto ec) mutable
	{
		REQUIRE(ec == std::errc::operation_canceled);
		done.set_value();
	});

	boost::asio::steady_timer timer{threads.get_executor(), 500ms};
	timer.async_wait([conn](auto)
	{
		// Time out! Let's cancel the async_connect()
		conn->close();
	});
	REQUIRE(done.wait_for(30s) == std::future_status::ready);

	threads.stop();
	threads.join();
}
