# Sanali

Redis client library for C++17/Boost ASIO

## Features

- Integrate with Boost ASIO.
- Intuitive C++20 format syntax.
- [Single header](include/Sanali.hh) only. Easy to integrate to your project.
- Fully asynchronous. Does not block any threads.
- Support pipelining by default.
- Memory-efficient for large strings.

## How to use Sanali

Simple Usage:

```cpp
#include <Sanali.hh>

// Connect to redis server
sanali::Redis redis{"localhost", "6379"};

// Issue command
redis.command([](auto&& resp, std::error_code ec)
{
    // "+OK"
    fmt::print("{}\n", resp);
    
// Setting "now123" to "2023-07-23 21:45:27.940861218"
}, "SET now{} {}", 123, std::chrono::system_clock::now());
```

The first argument of `Redis::command()` is a callback function which will be run after the command is executed.
The response from redis server is passed to the callback. The callback function will be run by the Boost ASIO executor
(typically `io_context`) passed in `Redis::Redis()`. The above example uses the default `boost::asio::system_executor`
as the executor, meaning that the callback functions will be run by an unspecified thread pool.

The second argument is the redis command string. It can contain "replacement fields" (e.g. `{}`) which will
be replaced by subsequence arguments. The syntax of the command string is the same as C++20 format or `{fmt}`,
with one exception only: positional and named argument are not supported. See the
[documentation](https://fmt.dev/latest/syntax.html) of `{fmt}` for details.

After the redis server executes the command, it will send back a response, which is passed to the callback function
as its first argument: `resp` in the above example. Its type is `Resp`, named after the [Redis serialization protocol]
(https://redis.io/docs/reference/protocol-spec/). It is actually an `std::variant` of a few types supported by redis:
integers, strings, boolean, "nil"s and arrays.

For Redis commands that supports variable number of arguments, it is not possible to specify all of them in a command
string. Therefore, we need to use a `CommandBuilder` to construct them. The constructor of `CommandBuilder` takes
a command string and a number of formatting arguments, just like `Redis::command()`. After constructing a
`CommandBuilder`, you can call `append()` to add any number of arguments to the redis command.

```cpp
CommandBuilder hset{"HSET some_hash first_key {}", first_value};
for (auto& [key, value] : some_map)
    hset.append("{}", key).append("This is the value: {}", value);

redis.command([](auto&& resp, auto ec){...}, std::move(hset));
```

Each call to `append()` will add exactly one argument to the Redis command. Unlike `CommandBuilder::CommandBuilder()`
and `Connection::command()`, the format string passed to `append()` is _not_ a command string, so spaces are not
treated specially. In other words, no matter how many spaces the first argument of `append()` contains, only one
argument will be added to the Redis command. 

If you want to receive a very large string from redis, you can specify a receiver call back function to receive data in
smaller chunks. The call back function will be called multiple times until all data have been received.

```cpp
redis.set_read_buffer_size(16*1024);
redis.command([output_file](void* data, std::size_t size, auto& info, std::error_code ec)
{
    if (ec)
    {
        fmt::print("Error encountered: {}, ec.message());
        return;
    }

    fmt::print("Received {} bytes at offset {}\n", size, info.offset);
    write(output_file, data, size);

    if (info.is_last)
        fmt::print("Finished data transfer {} bytes.\n", info.total_size);
}, "GET large_key");
```

The amount of data received in each call depends on the size of read buffer. In general, the data received
will always be smaller than or equal to the read buffer size. When using a receiver callback, there is no need to
allocate enough memory to store the whole string. This is useful because redis clients usually runs in the same host
as the redis server. It preferable to conserve memory usage even for redis clients, in order to reserve the precious
memory for the redis server to use.

When working with large keys, `boost::beast::multi_buffer` is more efficient for large (> 1MB)
amount of data than `std::string`, because it doesn't have to ensure continuous storage. Also,
`std::string` has to copy its content every time
when it grows. Therefore, prefer using `boost::beast::multi_buffer` instead of `std::string` when the key is very large.

```cpp
redis.command([&redis](boost::beast::multi_buffer&& value, std::error_code ec)
{
    redis.command[](auto&& ok, std::error_code ec)
	{
        // +OK
        fmt::print("Server response {} {}\n", ok, ec);

    // Note that "value" is moved instead of copied. It will be moved into
	// Sanali's internal buffers and sent to redis server. The data will not be copied
	// and there is no need to worry about its life-time either. Just don't use it
	// after std::move()'ing it.
	}, "SET anotherkey {}-and-some-suffix", std::move(value));
}, "GET large_key");
```

`CommandBuilder::append()` also supports `boost::beast::multi_buffer`. You can use them to build
Redis commands with variable number of arguments.

## Special Characters in Command Strings

The only special character in command strings is the space character ' '. Other characters, including other
white space characters like tabs, have no special meaning and will be treated as normal characters.
Therefore, it is safe to include the so-called "unsafe" data in redis command arguments. In Sanali, only the second
argument of `command()` (as well as the first argument of `CommandBuilder` constructor and `assign()`)
will be treated as command string and scanned for spaces. Strings passed as the 3rd argument and so on
are treated as data, so spaces inside them will _not_ be interpreted as separators in redis command.

```cpp
std::string field_name = "some field";
std::string some_value = "some values with spaces inside them";
redis.command([](auto&&, auto)
{
    // Set the hash field "some field" to be "some values with spaces inside them"
    // instead of setting the hash field "some" to be "field".
}, "HSET some_hash {} {}", user_input);
```

This is one of the advantage of the redis protocol: it is immune to injection attacks, even without the need of
any escaping:

```cpp
std::string user_input = "value; FLUSHALL"
redis.command([](auto&&, auto)
{
    // "key" is set to "value; FLUSHALL".
    // The "FLUSHALL" command will NOT be executed.
    // The content of "user_input" will ALWAYS be treated as data. It will never be parsed so there is no
	// need to "escape" them.
}, "SET key {}", user_input);
```

On the other hand, if the value or key have spaces, you must use a "{}" replacement and provide them by arguments.
If you put them in the command string, they will be interpreted as separators. Double quotes will not help: they
have no special meaning and will be interpreted as normal characters. 

Also, it is mandatory to use a C++ literal
string as the command string. Using `std::string`/`std::string_view` will cause a compile error:

```cpp
std::string user_input{"DEL important_key"};

// error: no matching function for call to ‘sanali::Connection::command
redis.command([&](auto&&, auto ec)
{
}, user_input);
```

Similarly, it is also an error if the first field has replacement field. An exception will be thrown.

```cpp
std::string user_input{"SET"};

// throws exception
conn->command([&](std::string&&, auto ec)
{
}, "H{} key value", user_input);
```

In short, command strings will always be double-quoted literal strings in the source code. It makes them easy
to spot in code reviews, and thus reducing the chances of mistakes.

## Advance Topics

- [Multi-threading issues](doc/threads.md)

## Download

Sanali is still in early development. It is not recommended for production. You can get the latest snapshot [here](include/Sanali.hh).

## Requirements

- [Boost 1.82](https://www.boost.org/users/history/version_1_82_0.html) or above
- [{fmt} 10.0.0](https://fmt.dev/10.0.0/) or above
- [Catch2 3.3.2](https://github.com/catchorg/Catch2) or above for unit test
